<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthApiController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\API\TaskApiController;
use App\Http\Controllers\API\SectionApiController;
use App\Http\Controllers\API\ProjectAPIController;
use App\Http\Controllers\API\UserAPIController;
use App\Http\Controllers\API\TagAPIController;
use App\Http\Controllers\API\DashboardApiController;
use App\Http\Controllers\API\NotiAPIController;
use App\Http\Controllers\API\InvitationAPIController;
use App\Http\Controllers\API\LogAPIController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->post('/user', function (Request $request) {
    return auth('sanctum')->user();
});


if (request('user_id')){

    // WITHOUT ACCESS TOKEN
    Route::controller(ProjectAPIController::class)->group(function(){
        Route::post('/getmyproj', 'getProjects');
    });

    Route::controller(SectionApiController::class)->group(function(){
        Route::post('/update_section_seq', 'updateSequence');
    });
    
    Route::controller(TaskApiController::class)->group(function(){
        Route::post('/taskdets', 'taskdets');
        Route::post('/update_task_seq', 'updateSequence');
        Route::post('/task_search', 'search');
    });
    
    Route::controller(ProjectAPIController::class)->group(function(){
        Route::post('/getboard', 'getBoard');
        Route::post('/getstat', 'getProjStat');
        Route::post('/getallproj', 'getAllProjects');
    });
    
    Route::controller(LogAPIController::class)->group(function(){
        Route::post('/getlog', 'index');
    });
    
    Route::controller(UserAPIController::class)->group(function(){
        Route::post('/get_user_dets', 'getTeam');
    });
    
    Route::controller(TagAPIController::class)->group(function(){
        Route::post('/get_tag_dets', 'getDetails');
    });
    
    Route::controller(DashboardApiController::class)->group(function(){
        Route::post('/get_dash', 'getDashboardStatistics');
        Route::post('/update_dash', 'updateDash');
    });
    Route::controller(NotiAPIController::class)->group(function(){
        Route::post('/getnotif', 'getNotif');
        Route::post('/readnotif', 'readNotif');
    });
    
    Route::controller(InvitationAPIController::class)->group(function(){
        Route::post('/getinvite', 'getInvite');
    });
    
    
    Route::controller(AuthApiController::class)->group(function(){
        Route::post('mob_authenticate', 'mob_authenticate');
        Route::post('mob_gen_token', 'mob_gen_token');
        Route::post('get_userdata', 'getuserdata');
        Route::post('get_settings', 'getsettings');
    });

}else{
     // WITH ACCESS TOKEN
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::controller(ProjectAPIController::class)->group(function(){
            Route::post('/getmyproj', 'getProjects');     
        });

        Route::controller(SectionApiController::class)->group(function(){
            Route::post('/update_section_seq', 'updateSequence');
        });
        
        Route::controller(TaskApiController::class)->group(function(){
            Route::post('/taskdets', 'taskdets');
            Route::post('/update_task_seq', 'updateSequence');
            Route::post('/task_search', 'search');
        });
        
        Route::controller(ProjectAPIController::class)->group(function(){
            Route::post('/getboard', 'getBoard');
            Route::post('/getstat', 'getProjStat');
            Route::post('/getallproj', 'getAllProjects');
        });
        
        Route::controller(LogAPIController::class)->group(function(){
            Route::post('/getlog', 'index');
        });
        
        Route::controller(UserAPIController::class)->group(function(){
            Route::post('/get_user_dets', 'getTeam');
        });
        
        Route::controller(TagAPIController::class)->group(function(){
            Route::post('/get_tag_dets', 'getDetails');
        });
        
        Route::controller(DashboardApiController::class)->group(function(){
            Route::post('/get_dash', 'getDashboardStatistics');
            Route::post('/update_dash', 'updateDash');
        });
        Route::controller(NotiAPIController::class)->group(function(){
            Route::post('/getnotif', 'getNotif');
            Route::post('/readnotif', 'readNotif');
        });
        
        Route::controller(InvitationAPIController::class)->group(function(){
            Route::post('/getinvite', 'getInvite');
        });
        
        
        Route::controller(AuthApiController::class)->group(function(){
            Route::post('mob_authenticate', 'mob_authenticate');
            Route::post('mob_gen_token', 'mob_gen_token');
            Route::post('get_userdata', 'getuserdata');
            Route::post('get_settings', 'getsettings');
        });
          
    });
}



