<?php

namespace App\Events;

use App\Models\Tag;
use App\Models\Task;
use App\Models\TaskTag;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskTagEvent implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $event, $task, $tag, $pivot;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event, $id)
    {
		$this->event = $event;
		$this->pivot = TaskTag::find($id);
		$this->task = Task::find($this->pivot->task_id);
		$this->tag = Tag::find($this->pivot->tag_id);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->task->project_id);
    }
}
