<?php

namespace App\Events;

use App\Models\Team;
use App\Models\User;
use App\Models\Project;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TeamEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	public $event, $data, $project, $team = null;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event, $user_id, $project_id)
    {
		$this->event = $event;
		$this->project = Project::find($project_id);
		$this->data = $user_id;
		if ($event == 'add-member' || $event == 'update-level') {
			$this->data = Project::with(['team' => function($query) use ($user_id) {
								$query->where('user_id', $user_id);
							}])->find($project_id)->team->first();
			$this->team = Team::where(['user_id' => $user_id, 'project_id' => $project_id])->first();
		}
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->project->id);
    }
}
