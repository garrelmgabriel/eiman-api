<?php

namespace App\Events;

use App\Models\Task;
use App\Models\User;
use App\Models\Watcher;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WatcherEvent implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $event, $task, $watcher, $pivot;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($event, $id)
    {
		$this->event = $event;
		$this->pivot = Watcher::find($id);
		$this->task = Task::find($this->pivot->task_id);
		$this->watcher = User::find($this->pivot->user_id);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->task->project_id);
    }
}
