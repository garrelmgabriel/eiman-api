<?php

namespace App\Events;

use App\Models\Task;
use App\Models\Attachment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AttachmentEvent implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $task, $attachments;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id)
    {
		$this->task = Task::with('project')->find($task_id);
		$this->attachments = Attachment::with('user')->where('task_id', $task_id)->get();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->task->project_id);
    }
}
