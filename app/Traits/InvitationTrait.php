<?php

namespace App\Traits;

use Mail;
use App\Models\User;
use App\Traits\TeamTrait;
use App\Models\Invitation;
use App\Events\UserInvitations;

trait InvitationTrait
{

    use TeamTrait;

    // send invitations to the given emails and store the records
    public function sendInvitation($projectId, $projectName, $emails, $message) {
        foreach ($emails as $email) {
            if ($this->isMember($projectId, $email)) {
                continue;
            }

            $user = User::where('email', $email)->first();

            $invitation = Invitation::create([
                'user_id' => auth()->user()->id,
                'recipient_id' => $user ? $user->id : null,
                'project_id' => $projectId,
                'email' => $email,
                'message' => $message,
                'expiration' => now()->addDays(3)->format('Y-m-d H:i:s'),
            ]);

			if ($user) {
				broadcast(new UserInvitations($invitation['id']))->toOthers();
			}

            $data = array(
                'id' => $invitation->id,
                'sender' => auth()->user()->firstname . ' ' . auth()->user()->lastname,
                'project' => $projectName,
                'message' => $message,
                'email' => $email,
                'subject' => 'Project Invitation',
                'date' => now()->isoFormat('MMM Do, YYYY'),
                'expiration' => now()->addDays(3)->isoFormat('MMM Do, YYYY'),
            );

            Mail::send(['html' => 'emails.invitation'], ['data' => $data], function ($message) use ($data) {
                $message->to($data['email'])->subject($data['subject']);
                $message->from(env('MAIL_FROM_ADDRESS', auth()->user()->email),env('MAIL_FROM_NAME', 'eiMAN Task Management'));
            });
        }
    }
}
