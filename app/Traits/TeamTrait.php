<?php

namespace App\Traits;

use App\Models\Log;
use App\Models\Team;
use App\Models\User;
use App\Events\TeamEvent;
use App\Traits\NotificationTrait;

trait TeamTrait
{
	use NotificationTrait;

    // check if a given email is already a part of a project
    public function isMember($projectId, $email)
    {
        // Check if email is already present in eiMAN users
        if (!User::where('email', $email)->exists()) {
            return false;
        }

        return User::with(['projects' => function($query) use ($projectId) {
                    $query->where('projects.id', $projectId);
                }])->where('email', $email)->first()->projects->count();
    }

    // store team member
    public function storeMember($projectId, $inviter = null)
    {
        $team = Team::where('user_id', auth()->user()->id)->latest()->first();

        $sequence = $team ? $team->sequence : 0;

        Team::create([
            'user_id' => auth()->user()->id,
            'project_id' => $projectId,
            'user_level' => $inviter ? 0 : 1,
            'sequence' => $sequence + 1
        ]);

		// If not owner | is invited, log the activity and send notification
		if ($inviter) {
			$activityRecord = [
                'user_id' => $inviter,
                'project_id' => $projectId,
                'model' => 'User',
                'model_id' => auth()->user()->id,
                'title' => 'Joined project.',
                'message' => 'added <strong class="text-capitalize">$model</strong> to the project.',
				'icon' => 'mdi-account-plus-outline',
                'event' => 'info'
			];

			// Log activity -- new project member
			Log::create($activityRecord);

			// Send notification -- new project member
			$activityRecord['message'] = 'added <strong class="text-capitalize">' . auth()->user()->full_name . '</strong> to the project.';
			$this->sendNotification($activityRecord, 'owner');

			broadcast(new TeamEvent('add-member', auth()->user()->id, $projectId))->toOthers();
		}
    }

	// get user's project membeship info
	public function getMembership($projectId, $userId) {
		return User::with(['membership' => function($query) use ($projectId) {
					$query->where('project_id', $projectId);
				}])->find($userId)->membership->first();
	}
}
