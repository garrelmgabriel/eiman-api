<?php

namespace App\Traits;

use App\Models\Task;
use App\Models\Section;
use App\Traits\LogTrait;

trait SectionTrait
{
	use LogTrait;

    // store sets of sections
    public function storeSections($projectId, $sections) {
        $sequence = 0;
        $sectionIds = [];
        foreach ($sections as $section) {
			$sectionname = preg_replace('/\s/u', ' ', $section['name']);

            if ($section['sequence']) {
                $sequence = $section['sequence'];
            }

            // update existing sections
            if ($section['id']) {
                Section::where('id', $section['id'])->update([
                    'name' => trim($sectionname),
                    'color' => $section['color'],
                ]);
                array_push($sectionIds, $section['id']);
            } else { // create new sections
                $newSection = Section::create([
                    'project_id' => $projectId,
                    'name' => trim($sectionname),
                    'color' => $section['color'],
                    'sequence' => $sequence + 1
                ]);
                array_push($sectionIds, $newSection->id);
				$this->createSectionLog($projectId, $newSection->id);
            }
        }

        // Delete all sections not included on the list
        $trashedSections = Section::whereNotIn('id', $sectionIds)->where('project_id', $projectId)->get();

		foreach ($trashedSections as $section) {
			Section::where('id', $section->id)->update(['status' => 'trashed']);
			Task::where('section_id', $section->id)->update(['visible' => false]);
			$this->updateSectionStatusLog($projectId, $section->id, 'trashed');
		}
    }
}
