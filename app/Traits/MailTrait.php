<?php

namespace App\Traits;

use Mail;

trait MailTrait
{
    // Send Mail
    public function sendMail($template, $data)
	{
        Mail::send(['html' => $template], ['data' => $data], function ($message) use ($data) {
            $message->to($data['email'])->subject($data['subject']);
            $message->from(env('MAIL_FROM_ADDRESS', 'eimanagement2021@gmail.com'),env('MAIL_FROM_NAME', 'eiMAN - Task Management System'));
        });
    }
}
