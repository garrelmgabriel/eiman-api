<?php

namespace App\Traits;

use App\Models\AnnouncementUser;
use App\Traits\NotificationTrait;

trait AnnouncementTrait
{
	use NotificationTrait;

    // manage attendees of project announcement
    public function manageAttendees($projectId, $annoucementId, $attendees, $reminder) {
		$attendees = is_array($attendees) ? $attendees : explode(',', $attendees);
		for ($i=0; $i < count($attendees); $i++) {
			$title = 'Added an attendee.';
			$message = 'added you to a meeting.';

			 // update existing attendees
			 if (AnnouncementUser::where(['announcement_id' => $annoucementId, 'user_id' =>  $attendees[$i]])->exists()) {
                AnnouncementUser::where(['announcement_id' => $annoucementId, 'user_id' =>  $attendees[$i]])->update([
                    'is_creator' => $attendees[$i] == auth()->user()->id ? 1 : 0,
                    'alarm_reminder' => $reminder,
                ]);
				$title = 'Updated project meeting.';
				$message = 'updated project meeting details.';
            } else { // create new attendees
                AnnouncementUser::create([
                    'announcement_id' => $annoucementId,
                    'user_id' =>  $attendees[$i],
                    'is_creator' => $attendees[$i] == auth()->user()->id ? 1 : 0,
                    'alarm_reminder' => $reminder
                ]);
            }

			// notify attendees
			$this->sendNotification([
				'project_id' => $projectId,
				'user_id' => auth()->user()->id,
				'recipient_id' => $attendees[$i],
				'title' => $title,
				'message' => $message,
				'icon' => 'mdi-bullhorn-outline',
				'event' => 'info'
			]);
		}

        // Delete all sections not included on the list
        $removedAttendees = AnnouncementUser::whereNotIn('user_id', $attendees)->where('announcement_id', $annoucementId)->get();

		foreach ($removedAttendees as $attendee) {
			AnnouncementUser::destroy($attendee->id);

			// notify removed attendees
			$this->sendNotification([
				'project_id' => $projectId,
				'user_id' => auth()->user()->id,
				'recipient_id' => $attendee->user_id,
				'title' => 'Removed an attendee.',
				'message' => 'removed you from a meeting.',
				'icon' => 'mdi-bullhorn-outline',
				'event' => 'danger'
			]);
		}
    }
}
