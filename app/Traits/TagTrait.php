<?php

namespace App\Traits;

use App\Models\Tag;
use App\Models\TaskTag;
use App\Events\TagEvent;

trait TagTrait
{
    // store sets of tags
    public function storeTags($projectId, $tags) {
        $tagIds = [];
        foreach ($tags as $tag) {
			$tagname = preg_replace('/\s/u', ' ', $tag['name']);
            // update existing tags
            if ($tag['id']) {
                Tag::where('id', $tag['id'])->update([
                    'name' => trim($tagname),
                    'color' => $tag['color'],
                ]);
                array_push($tagIds, $tag['id']);
				broadcast(new TagEvent('update-tag', $tag['id']))->toOthers();
            } else { // create new tags
                $t = Tag::create([
                    'project_id' => $projectId,
                    'name' => trim($tagname),
                    'color' => $tag['color'],
                ]);
                array_push($tagIds, $t->id);
            }
        }

        // Delete all tags not included on the list
        $trashedTags = Tag::whereNotIn('id', $tagIds)->where('project_id', $projectId)->get();

		foreach ($trashedTags as $tag) {
			broadcast(new TagEvent('delete-tag', $tag->id))->toOthers();
			Tag::destroy($tag->id);
			TaskTag::where('tag_id', $tag->id)->delete();
		}
    }
}
