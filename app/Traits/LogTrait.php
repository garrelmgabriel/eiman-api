<?php

namespace App\Traits;

use App\Models\Log;

trait LogTrait
{
    // log creation of section
    public function createSectionLog($projectId, $sectionId) {
		// Log activity -- convert checklist to task
		Log::create([
			'user_id' => auth()->user()->id,
			'project_id' => $projectId,
            'model' => 'Section',
            'model_id' => $sectionId,
			'title' => 'Created section.',
            'message' => 'created <strong>$model</strong> section.',
			'icon' => 'mdi-bulletin-board',
			'event' => 'success'
		]);
    }

    // log deletion of section
    public function updateSectionStatusLog($projectId, $sectionId, $status) {
		// Log activity -- convert checklist to task
		Log::create([
			'user_id' => auth()->user()->id,
			'project_id' => $projectId,
            'model' => 'Section',
            'model_id' => $sectionId,
			'title' => ($status == 'active' ? 'Restored' : 'Trashed') . ' section.',
            'message' => ($status == 'active' ? 'restored' : 'trashed') . ' <strong>$model</strong> section.',
			'icon' => 'mdi-bulletin-board',
			'event' => ($status == 'active' ? 'info' : 'danger')
		]);
    }
}
