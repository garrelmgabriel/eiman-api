<?php

namespace App\Traits;

use App\Models\Task;
use App\Models\User;
use App\Models\Project;
use App\Models\Section;
use App\Traits\TeamTrait;

trait TaskTrait
{
	use TeamTrait;

    // get task with basic data, for task card display
    public function parseBasicTask($task)
    {
        $id = $task->id;

        $task['project'] = Project::where('id', $task->project_id)->first(['id', 'token', 'name']);
        $task['section'] = Section::where('id', $task->section_id)->first(['id', 'name', 'color']);
        $task['pin'] = [];
        $task['tags'] = Task::with('tags')->find($id)->tags;
        $task['completed_checklists'] = Task::with(['checklists' => function ($query) {
                                                $query->where('checklists.status', 'checked');
                                            }])->find($id)->checklists->count();
        $task['total_checklists'] = Task::with('checklists')->find($id)->checklists->count();
        $task['attachments'] = Task::with('attachments')->find($id)->attachments->count();
        $task['comments'] = Task::with('comments')->find($id)->comments->count();
        $task['assignees'] = Task::with('assignees')->find($id)->assignees;

        // Get the role of the logged in user
        $task['membership'] = $this->getMembership($task->project_id, auth()->user()->id);

        return $task;
    }

    // get task with basic data, for task modal display
    public function parseAdvanceTask($task)
    {
        $id = $task->id;

        $task['project'] = Project::where('id', $task->project_id)->first(['id', 'token', 'name']);
        $task['section'] = Section::where('id', $task->section_id)->first(['id', 'name', 'color']);
        $task['pin'] = [];
        $task['tags'] = Task::with('tags')->find($id)->tags;
        $task['completed_checklists'] = Task::with(['checklists' => function ($query) {
                                                $query->where('checklists.status', 'checked');
                                            }])->find($id)->checklists->count();
        $task['checklists'] = Task::with(['checklists' => function($query) {
                                    $query->orderBy('sequence', 'ASC');
                                }])->find($id)->checklists;
        $task['attachments'] = Task::with('attachments.user')->find($id)->attachments;
        $task['comments'] = Task::with('comments.user')->find($id)->comments;
        $task['assignees'] = Task::with('assignees')->find($id)->assignees;
        $task['watchers'] = Task::with('watchers')->find($id)->watchers;

        return $task;
    }

    // get tasks of a given section
    public function getSectionTasks($sectionId, $filters = null)
    {
        $tasks = Section::with(['tasks' => function($query) use ($filters) {
                        $query->when($filters, function ($query) use ($filters) {
                                return $query->where('tasks.status', $filters['task_status']);
                            })->when(!$filters, function ($query) use ($filters) {
                                return $query->whereNotIn('tasks.status', ['trashed', 'archived']);
                            })->orderBy('sequence', 'ASC');
                    }])->find($sectionId)->tasks;

        foreach ($tasks as $key => $task) {
            $tasks[$key] = $this->parseBasicTask($task);
        }

        return $tasks;
    }

    // get tasks of a project
    public function getProjectTasks($id)
    {
        //
    }
}
