<?php

namespace App\Traits;

use App\Models\Team;
use App\Models\Watcher;
use App\Models\Assignment;
use App\Models\Notification;
use App\Events\UserNotifications;

trait NotificationTrait
{
    // send notification
    public function sendNotification($fields, $recipientType = null, $excluded = []) {
		$recipients = [];

		if ($recipientType) {
			if ($fields['user_id']) { array_push($excluded, $fields['user_id']); }
			$recipients = $this->getRecipients(
				$recipientType,
				$fields['project_id'],
				$recipientType == 'owner' ? null : $fields['task_id'],
				$excluded
			);

			if ($recipients) {
				foreach ($recipients as $key => $value) {
					$fields['recipient_id'] = $value;
					$notif = Notification::create($fields);
					broadcast(new UserNotifications($notif['id']))->toOthers();
				}
			}
		} else {
			if ($fields['user_id'] != $fields['recipient_id']) {
				$notif = Notification::create($fields);
                broadcast(new UserNotifications($notif['id']))->toOthers();
			}
		}
    }

	public function getRecipients($type, $projectId, $taskId, $excluded = null) {
		$recipients = [];

		if ($type == 'owner') {
			$owner = Team::where(['project_id' => $projectId, 'user_level' => 1])->first();
			$recipients = [$owner->user_id];
		} else if ($type == 'group') {
			$assignees = Assignment::where('task_id', $taskId)->pluck('user_id')->toArray();
			$recipients = array_merge($recipients, $assignees);

			$watchers = Watcher::where('task_id', $taskId)->pluck('user_id')->toArray();
			$recipients = array_merge($recipients, $watchers);
		} else if ($type == 'all') {
			$owner = Team::where(['project_id' => $projectId, 'user_level' => 1])->first();
			$recipients = [$owner->user_id];

			$assignees = Assignment::where('task_id', $taskId)->pluck('user_id')->toArray();
			$recipients = array_merge($recipients, $assignees);

			$watchers = Watcher::where('task_id', $taskId)->pluck('user_id')->toArray();
			$recipients = array_merge($recipients, $watchers);
		}

		// remove redundant ids
		$recipients = array_unique($recipients);

		// exclude indicated recipients from receiving notifications
		if ($excluded) {
			foreach ($excluded as $user) {
				if (($key = array_search($user, $recipients)) !== false) {
					unset($recipients[$key]);
				}
			}
		}

		return $recipients;
	}
}
