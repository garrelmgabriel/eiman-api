<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$filters = request()->all();

        $new = Notification::where(['recipient_id' => auth()->user()->id, 'is_read' => null])
							->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
								$query->where('project_id', $filters['project-id']);
							})->count();
		$notifications = Notification::when($filters['notif-filter'], function ($query) use ($filters) {
								$query->where('title', 'LIKE', '%' . $filters['notif-filter'] . '%');
							})->when($filters['notif-status-filter'], function ($query) use ($filters) {
								if ($filters['notif-status-filter'] == 'unread') {
									$query->whereNull('is_read');
								} else {
									$query->whereNotNull('is_read');
								}
							})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
								$query->where('project_id', $filters['project-id']);
							})
							->where('recipient_id', auth()->user()->id)
							->orderBy('created_at', 'DESC')->paginate(15);

		return ['new' => $new, 'notifications' => $notifications];
    }

	/**
	 * Count listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function count()
	{
		$project = request('project-id');

		return Notification::where(['recipient_id' => auth()->user()->id, 'is_read' => null])
							->when($project, function ($query) use ($project) { // project notifs
								$query->where('project_id', $project);
							})->count();
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        return Notification::where('id', request('id'))->update(['is_read' => Carbon::now()->format('Y-m-d H:i:s')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
		$filters = request()->all();
        $message = '';
        if ($filters['id']) {
            if ($filters['action'] == 'read') { // mark notification as read
				Notification::where('id', $filters['id'])->update(['is_read' => Carbon::now()->format('Y-m-d H:i:s')]);
                $message = 'Notification Marked as Read!';
            } else if ($filters['action'] == 'unread') { // mark notification as unread
				Notification::where('id', $filters['id'])->update(['is_read' => null]);
                $message = 'Notification Marked as Unread!';
            } else { // delete a notification
                Notification::destroy($filters['id']);
                $message = 'Notification Deleted Successfully!';
            }
        } else {
            if ($filters['action'] == 'read') { // read all unread notifications
				Notification::when($filters['notif-filter'], function ($query) use ($filters) {
									$query->where('title', 'LIKE', '%' . $filters['notif-filter'] . '%');
								})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
									$query->where('project_id', $filters['project-id']);
								})
								->where('recipient_id', auth()->user()->id)
								->whereNull('is_read')
								->update(['is_read' => Carbon::now()->format('Y-m-d H:i:s')]);

				$message = 'Notifications Marked as Read!';
            } else if ($filters['action'] == 'unread') { // unread all read notifications
				Notification::when($filters['notif-filter'], function ($query) use ($filters) {
									$query->where('title', 'LIKE', '%' . $filters['notif-filter'] . '%');
								})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
									$query->where('project_id', $filters['project-id']);
								})
								->where('recipient_id', auth()->user()->id)
								->whereNotNull('is_read')
								->update(['is_read' => null]);

				$message = 'Notifications Marked as Unead!';
            } else { // delete all notifications (all | read)
				Notification::when($filters['notif-filter'], function ($query) use ($filters) {
									$query->where('title', 'LIKE', '%' . $filters['notif-filter'] . '%');
								})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
									$query->where('project_id', $filters['project-id']);
								})->when($filters['action'] == 'delete-read', function ($query) use ($filters) {
									$query->whereNotNull('is_read');
								})
								->where('recipient_id', auth()->user()->id)
								->delete();

                $message = 'Notifications Deleted Successfully!';
            }
        }

        $new = Notification::where(['recipient_id' => auth()->user()->id, 'is_read' => null])->count();

        return ['status' => 'success', 'message' => $message, 'data' => $new];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
