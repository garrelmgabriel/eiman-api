<?php

namespace App\Http\Controllers;

use Image;
use App\Models\Log;
use App\Models\Task;
use App\Models\Attachment;
use Illuminate\Http\Request;
use App\Events\AttachmentEvent;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
	use NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return Attachment::where('task_id', request('task-id'))->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validExt = array('jpg','jpeg','png','svg','webp','psd','doc','docx','ppt','pub','pptx','pdf','xlsx','xls','xlx','csv','sql','txt','zip','rar');

        if($request->totalAttachments > 0) {
            $attachments = [];
            $task = Task::find($request->taskId);

			try {
				DB::beginTransaction();

				for ($x = 0; $x < $request->totalAttachments; $x++) {
					if ($request->hasFile('attachments'.$x)) {
						$file = $request->file('attachments'.$x);
						$fileName = 'attachment_' . md5(mt_rand(1, 20) . $file->getClientOriginalName()) . date('Ymdgi');
						$ext = explode('.', $file->getClientOriginalName());
						$ext = end($ext);

						// Check file format
						if (!in_array($ext, $validExt)) {
							return response()->json(['errors' => 'Invalid file format.', 'message' => 'You can only upload images, pdf, pub, ppt, xls, and doc files.'], 413);
						}

						// Check file size
						if ($file->getSize() > 26214400) {
							return response()->json(['errors' => 'File size too large.', 'message' => 'You can only upload files less than 25MB.'], 413);
						}

						$name = $fileName . '.' . $ext;
						$path = $file->storeAs('public/attachments/' . $task->project_id . '/task_' . $request->taskId, $name);

						// create thumbnail
						if (in_array($ext, ['jpg', 'jpeg', 'png'])) {
							$directory = 'public/thumbnail_attachments/' . $task->project_id . '/task_' . $request->taskId;
							Storage::makeDirectory($directory);

							$img = Image::make($file->getRealPath())->resize(null, 80, function ($constraint) {
								$constraint->aspectRatio();
							});
							$img->save(storage_path('app') . '/' . $directory . '/' . $name);
						}

						if (Storage::disk('local')->exists($path)) {
							$attachment = Attachment::create([
								'user_id' => auth()->user()->id,
								'task_id' => $request->taskId,
								'name' => $name,
								'title' => $file->getClientOriginalName(),
								'path' => 'attachments/' . $task->project_id . '/task_' . $request->taskId . '/' . $name,
								'extension' => $ext,
							]);

							$attachments[$x] = Attachment::with('user')->find($attachment->id);

							$activityRecord = [
								'user_id' => auth()->user()->id,
								'project_id' => $task->project_id,
								'task_id' => $request->taskId,
								'title' => 'Uploaded an attachment.',
								'message' => 'attached <strong>' . $file->getClientOriginalName() . '</strong> file to <strong>$task_name</strong> task.',
								'icon' => 'mdi-file-plus-outline',
								'event' => 'info'
							];

							// Log activity -- upload an attachment
							Log::create($activityRecord);

							// Send notification -- upload an attachment
							$activityRecord['message'] = 'attached <strong>' . $file->getClientOriginalName() . '</strong> file to <strong>' . $task->title . '</strong> task.';
							$this->sendNotification($activityRecord, 'group');
						}
					}
				}

				broadcast(new AttachmentEvent($task->id))->toOthers();
				DB::commit();
			} catch (Throwable $th) {
				DB::rollback();
				return $th;
			}

            return ['status' => 'success', 'message' => 'Attachment Uploaded Successfully!', 'data' =>  $attachments];
        } else {
            return ['status' => 'error', 'message' => 'Failed uploading attachment!'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public function show(Attachment $attachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public function edit(Attachment $attachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attachment $attachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attachment $attachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attachment $attachment)
    {
        try {
            DB::beginTransaction();

			$attachment = Attachment::find(request('id'));

			$task = Attachment::with('task')->find(request('id'))->task;

			Attachment::destroy(request('id'));
			broadcast(new AttachmentEvent($task->id))->toOthers();

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'title' => 'Removed an attachment.',
				'message' => 'removed <strong>' . $attachment->title . '</strong> file from <strong>$task_name</strong> task.',
				'icon' => 'mdi-file-remove-outline',
				'event' => 'danger'
			];

			// Log activity -- remove an attachment
			Log::create($activityRecord);

			// Send notification -- remove an attachment
			$activityRecord['message'] = 'removed <strong>' . $attachment->title . '</strong> file from <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group');

			Storage::disk('local')->delete('public/' . $attachment->path);
			Storage::disk('local')->delete('public/thumbnail_' . $attachment->path);

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Attachment Deleted Successfully!', 'data' => $task->id];
    }
}
