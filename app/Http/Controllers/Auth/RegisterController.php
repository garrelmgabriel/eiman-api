<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserSetting;
use App\Models\DashboardSetting;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'first-name' => ['required', 'regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
			'middle-name' => ['nullable','regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
			'last-name' => ['required', 'regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
			'email' => ['required', 'string', 'email', 'regex:/(.*)gmail\.com|scheirman\.ph|rsrealty\.ph|mnleistung\.de|travellersinsuranceph\.com|gmx\.net$/i', 'max:255', 'unique:users'],
			'password' => ['required', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/', 'string', 'min:6', 'confirmed'],
			'terms' => ['accepted'],
		],
		[
			'email.regex' => 'We appreciate your interest on using our system. However, at the moment we offer this service only to specific companies!',
			'password.regex' => 'Password must contain numbers and UPPERCASE/lowercase letters.',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\Models\User
	 */
	protected function create(array $data)
	{
		$company_id = 1;

		if (str_contains($data['email'], 'mnleistung.de')) {
			$company_id = 2;
		}

		$user = User::create([
			'company_id' => $company_id,
			'firstname' => $data['first-name'],
			'middlename' => $data['middle-name'],
			'lastname' => $data['last-name'],
			'email' => $data['email'],
			'password' => Hash::make($data['password']),
			'agree_to_terms' => 1,
		]);

		DashboardSetting::create(['user_id' => $user->id, 'statistics' => ["stat_tasks_today", "stat_total_tasks", "stat_active_tasks", "stat_completed_tasks" ]]);

		UserSetting::create(['user_id' => $user->id]);

		return $user;
	}
}
