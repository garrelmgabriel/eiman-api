<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Task;
use App\Models\User;
use App\Traits\MailTrait;
use App\Models\Assignment;
use App\Models\UserSetting;
use Illuminate\Http\Request;
use App\Events\AssignmentEvent;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class AssignmentController extends Controller
{
	use NotificationTrait, MailTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Assignment::where(['user_id' => request('user-id'), 'task_id' => request('task-id')])->exists()) {
            return response()->json(['errors' => 'User was already assigned to this task.'], 403);
        }

        try {
            DB::beginTransaction();

			$assignment = Assignment::create([
				'user_id' => request('user-id'),
				'task_id' => request('task-id'),
				'is_new' => request('user-id') == auth()->user()->id ? 0 : 1
			]);

			$assignee = Task::with(['assignees' => function($query) use ($assignment) {
							$query->where('assignments.id', $assignment->id);
						}])->find(request('task-id'))->assignees;

			$task = Task::find(request('task-id'));

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'User',
				'model_id' => request('user-id'),
				'title' => 'Assigned a user.',
				'message' => 'assigned <strong class="text-capitalize">$model</strong> to <strong>$task_name</strong> task.',
				'icon' => 'mdi-account-plus-outline',
				'event' => 'info'
			];

			// Log activity -- assign a user
			Log::create($activityRecord);

			// Send notification -- assign a user
			$activityRecord['message'] = 'assigned you to <strong>' . $task->title . '</strong> task.';
			$activityRecord['recipient_id'] = request('user-id');
			$this->sendNotification($activityRecord);
			broadcast(new AssignmentEvent('assign-user', $assignment->id))->toOthers();

			// send email if enabled
			if (request('user-id') != auth()->user()->id) {
				$user = User::find(request('user-id'));
				if ($user->settings->emails_for_task_assignments) {
					$data = array(
						'project_token' => $task->project->token,
						'task_token' => $task->token,
						'message' => '<strong style="text-transform: capitalize;">' . auth()->user()->display_name . '</strong> assigned you on <strong>"' . $task->title . '"</strong> task.',
						'email' => $user->email,
						'subject' => 'New Task Assignment'
					);

					$this->sendMail('emails.assignment', $data);
				}
			}

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Assigned User Successfully!', 'data' =>  $assignee[0]];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        //
    }

    /**
     * Transfer all assigned tasks to specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transfertTasks()
    {
        $assignations = User::with(['assigned' => function($query) {
			$query->whereNotIn('status', ['archived', 'trashed'])->where('project_id', request('project-id'));
		}])->find(request('transferor'))->assigned;

		$assigned = [];

        try {
            DB::beginTransaction();

			foreach ($assignations as $assignation) {
				// checks if the transferee is already assigned to task then delete the record
				if (Assignment::where(['task_id' => $assignation->id, 'user_id' => request('transferee')])->exists()) {
					Assignment::destroy($assignation->pivot->id);

					// Log activity -- unassign task
					Log::create([
						'user_id' => auth()->user()->id,
						'project_id' => request('project-id'),
						'task_id' => $assignation->id,
						'model' => 'User',
						'model_id' => request('transferor'),
						'title' => 'Unassigned a user.',
						'message' => 'unassigned <strong class="text-capitalize">$model</strong> from <strong>$task_name</strong> task.',
						'icon' => 'mdi-account-remove-outline',
						'event' => 'danger'
					]);

					continue;
				}

				// transfer task
				Assignment::where('id', $assignation->pivot->id)->update(['user_id' => request('transferee')]);

				// Log activity -- transfer task
				Log::create([
					'user_id' => auth()->user()->id,
					'project_id' => request('project-id'),
					'task_id' => $assignation->id,
					'model' => 'User',
					'model_id' => request('transferee'),
					'title' => 'Assigned a user.',
					'message' => 'transferred <strong>$task_name</strong> task to <strong class="text-capitalize">$model</strong>.',
					'icon' => 'mdi-account-switch-outline',
					'event' => 'info'
				]);
			}

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Transferred Tasks Successfully!', 'data' =>  ['tasks' => $assignations, 'assignee' => User::find(request('transferee'))]];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        try {
            DB::beginTransaction();

			$assignee = Assignment::find(request('id'));
			broadcast(new AssignmentEvent('unassign-user', request('id')))->toOthers();
			Assignment::destroy(request('id'));

			$task = Task::find($assignee->task_id);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'User',
				'model_id' => $assignee->user_id,
				'title' => 'Unassigned a user.',
				'message' => 'unassigned <strong class="text-capitalize">$model</strong> from <strong>$task_name</strong> task.',
				'icon' => 'mdi-account-remove-outline',
				'event' => 'danger'
			];

			// Log activity -- unassign a user
			Log::create($activityRecord);

			// Send notification -- unassign a user
			$activityRecord['message'] = 'unassigned you from <strong>' . $task->title . '</strong> task.';
			$activityRecord['recipient_id'] = $assignee->user_id;
			$this->sendNotification($activityRecord);

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Unassigned User Successfully!', 'data' => $assignee];
    }
}
