<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// accessible only for admin and super admin
        if (auth()->user()->user_level == 'user') {
            return view('errors/403');
        }

        return view('feedback');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$feedback = Feedback::create([
            'user_id' => auth()->user()->id,
            'message' => request('feedback-message'),
			'anonymous' => request('anonymous') ? true : false,
        ]);

        $data = array(
            'id' => $feedback->id,
            'name' => request('anonymous') ? 'Anonymous' :ucwords(auth()->user()->full_name),
            'feedback' => request('feedback-message'),
            'date' => Carbon::now()->toDayDateTimeString(),
            'email' => request('anonymous') ? 'no-reply@gmail.com' : auth()->user()->email
        );

        Mail::send(['html'=>'emails.feedback'], ['data' => $data], function($message) use ($data) {
            $message->to(env('MAIL_FROM_ADDRESS', 'eimanagement2021@gmail.com'))->subject('eiMan Feedback');
            $message->from($data['email'], $data['name']);
        });
    }
	public function getFeedback(){
		$feedback = Feedback::with('user')->get();
		return response()->json($feedback);
	}
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        //
    }
}
