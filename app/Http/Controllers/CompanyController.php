<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Traits\MailTrait;
use App\Models\UserSetting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DashboardSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
	use MailTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// accessible only for admin and super admin
        if (auth()->user()->user_level != 'superadmin') {
            return view('errors/403');
        }

        $statistics = [];

		$statistics['total'] = Company::count();
		$statistics['pro'] = Company::where('package_id', 1)->count();
		$statistics['business'] = Company::where('package_id', 2)->count();
		$statistics['enterprise'] = Company::where('package_id', 3)->count();

        return view('companies')->with('statistics', $statistics);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list()
	{
		$package = request('package');
		$name = request('name');
		$pagination = request('pagination');

		return Company::with('package')->when($name, function ($query) use ($name) {
						$query->orWhere('name', 'LIKE', '%' . $name . '%');
					})->when($package != 'all', function ($query) use ($package) {
						$query->where('package_id', $package);
					})->paginate($pagination);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'company-name' => 'required|string|max:50',
            'contact' => 'required|string|regex:/^\d{3}\-\d{3}\-\d{4}$/|unique:companies',
            'email' => 'required|string|email|regex:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/|unique:companies',
			'first-name' => 'required|regex:/^[ÑñA-Za-z\s\-]+$/|string|max:50',
			'middle-name' => 'nullable|regex:/^[ÑñA-Za-z\s\-]+$/|string|max:50',
			'last-name' => 'required|regex:/^[ÑñA-Za-z\s\-]+$/|string|max:50',
			'admin-email' => 'required|string|email|regex:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/|max:255|unique:users,email',
        ]);

        try {
            DB::beginTransaction();

			$company = Company::create([
				'package_id' => request('package'),
				'name' => request('company-name'),
				'slug' => preg_replace('/\s+/', '-', strtolower(request('company-name'))),
				'description' => request('description'),
				'contact' => request('contact'),
				'email' => request('email'),
				'subscription_duration' => request('duration'),
				'subscription_date' => Carbon::now()->format('Y-m-d H:i:s'),
				'subscription_expiration' => Carbon::now()->addMonths(request('duration'))->format('Y-m-d H:i:s'),
			]);

			$password = 'Pass_' . Str::random(5);
			$user = User::create([
				'company_id' => $company->id,
				'firstname' => request('first-name'),
				'middlename' => request('middle-name'),
				'lastname' => request('last-name'),
				'email' => request('admin-email'),
				'password' => Hash::make($password),
				'user_level' => 'admin',
				'agree_to_terms' => 1,
				'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
			]);

			DashboardSetting::create(['user_id' => $user->id, 'statistics' => ["stat_tasks_today", "stat_total_tasks", "stat_active_tasks", "stat_completed_tasks" ]]);

			UserSetting::create(['user_id' => $user->id]);

			// send email to company's email
			$data = array(
				'email' => $company->email,
				'name' => $company->name,
				'code' => $company->slug,
				'package' => $company->package->name,
				'admin_email' => $user->email,
				'password' => $password,
				'subscription_date' => Carbon::parse($company->subscription_date)->isoFormat('MMM DD, YYYY'),
				'subscription_expiration' => Carbon::parse($company->subscription_expiration)->isoFormat('MMM DD, YYYY'),
				'subject' => 'eiMAN Subscription'
			);

			$this->sendMail('emails.subscription', $data);

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

		return ['status' => 'success', 'message' => 'Company Created Successfully!', 'data' => $request->all()];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Company::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $validated = $request->validate([
            'company-name' => 'required|string|max:50',
            'contact' => 'required|string|regex:/^\d{3}\-\d{3}\-\d{4}$/|unique:companies,contact,' . request('company-id'),
            'email' => 'required|string|email|regex:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/|unique:companies,email,' . request('company-id'),
        ]);

		Company::where('id', request('company-id'))->update([
			'name' => request('company-name'),
			'slug' => preg_replace('/\s+/', '-', strtolower(request('company-name'))),
			'description' => request('description'),
			'contact' => request('contact'),
			'email' => request('email'),
		]);

		return ['status' => 'success', 'message' => 'Company Updated Successfully!', 'data' => Company::find(request('company-id'))];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
