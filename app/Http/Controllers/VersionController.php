<?php

namespace App\Http\Controllers;

use Image;
use App\Models\Log;
use App\Models\Tag;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Comment;
use App\Models\Company;
use App\Models\Project;
use App\Models\Section;
use App\Models\TaskTag;
use App\Models\Watcher;
use App\Models\Checklist;
use App\Models\Assignment;
use App\Models\Attachment;
use App\Models\UserSetting;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\DashboardSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class VersionController extends Controller
{
	/**
	 * Transfer users, user's settings and dashboard settings
	 */
	public function transferUsers()
	{
		$users = DB::connection('live')->table('users')
				->join('settings', 'users.id', '=', 'settings.user_id')
				->join('dashboard_settings', 'users.id', '=', 'dashboard_settings.user_id')
				->select('*', 'settings.id as settings_id', 'dashboard_settings.id as dashboard_settings_id')->orderBy('users.id', 'asc')->get();

		foreach ($users as $user) {
			$company_id = 1;
			if (str_contains($user->email, 'mnleistung.de')) {
				$company_id = 2;
			} else if (str_contains($user->email, 'scheirman.ph')) {
				$company_id = 3;
			} else if (str_contains($user->email, 'travellersinsuranceph.com')) {
				$company_id = 4;
			}
			User::create([
				'id' => $user->user_id,
				'company_id' => $company_id,
				'firstname' => $user->firstname,
				'middlename' => $user->middlename,
				'lastname' => $user->lastname,
				'nickname' => $user->nickname,
				'email' => $user->email,
				'password' => $user->password,
				'activity_status' => $user->nickname == 'DISABLED ACCOUNT' ? 3 : 0,
				'agree_to_terms' => $user->agree_to_terms,
				'status' => $user->nickname == 'DISABLED ACCOUNT' ? 'disabled' : 'active',
				'email_verified_at' => $user->email_verified_at,
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
			]);

			UserSetting::create([
				'id' => $user->settings_id,
				'user_id' => $user->user_id,
				'toast_alerts' => $user->toast_alerts,
				'emails_for_project_invitations' => $user->emails_for_project_invitations,
				'emails_for_task_assignments' => $user->emails_for_task_assignments,
				'emails_for_comments_or_mentions' => $user->emails_for_comments_or_mentions,
				'emails_for_reminders' => $user->emails_for_reminders,
				'emails_for_system_updates' => $user->emails_for_system_updates,
				'desktop_notif' => $user->desktop_notif,
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
			]);

			DashboardSetting::create([
				'id' => $user->dashboard_settings_id,
				'user_id' => $user->user_id,
				'statistics' => ["stat_tasks_today", "stat_total_tasks", "stat_active_tasks", "stat_completed_tasks" ],
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
			]);
		}

		return $users;
	}

	/**
	 * Transfer projects
	 */
	public function transferProjects()
	{
		$projects = DB::connection('live')->table('projects')
					->select('*')->orderBy('projects.id', 'asc')->get();

		foreach ($projects as $project) {
			Project::create([
				'id' => $project->id,
				'token' => substr(md5($project->id . date('Ymdgi') . $project->name), 0, 8),
				'name' => $project->name,
				'description' => $project->description,
				'icon' => 'fas fa-layer-group',
				'status' => $project->status,
				'created_at' => $project->created_at,
				'updated_at' => $project->updated_at,
			]);
		}

		return $projects;
	}

	/**
	 * Transfer teams
	 */
	public function transferTeams()
	{
		$teams = DB::connection('live')->table('teams')
					->select('*')->orderBy('teams.id', 'asc')->get();

		foreach ($teams as $team) {
			Team::create([
				'id' => $team->id,
				'user_id' => $team->user_id,
				'project_id' => $team->project_id,
				'user_level' => $team->user_level,
				'created_at' => $team->created_at,
				'updated_at' => $team->updated_at,
			]);
		}

		return $teams;
	}

	/**
	 * Transfer sections
	 */
	public function transferSections()
	{
		$sections = DB::connection('live')->table('sections')
					->select('*')->orderBy('sections.id', 'asc')->get();

		foreach ($sections as $section) {
			Section::create((array) $section);
		}

		return $sections;
	}

	/**
	 * Transfer tags
	 */
	public function transferTags()
	{
		$tags = DB::connection('live')->table('tags')
					->select('*')->orderBy('tags.id', 'asc')->get();

		foreach ($tags as $tag) {
			Tag::create((array) $tag);
		}

		return $tags;
	}

	/**
	 * Transfer tasks
	 */
	public function transferTasks()
	{
        set_time_limit(0);

		$tasks = DB::connection('live')->table('tasks')
					->select('*')->orderBy('tasks.id', 'asc')->get();

		foreach ($tasks as $task) {
			Task::create([
				'id' => $task->id,
				'project_id' => $task->project_id,
				'section_id' => $task->section_id,
				'sequence' => $task->sequence,
				'token' => substr(md5($task->id . date('Ymdgi') . $task->title), 0, 8),
				'title' => $task->title,
				'description' => $task->description,
				'start_date' => $task->start_date,
				'due_date' => $task->due_date,
				'status' => $task->status,
				'completed_at' => $task->completed_at,
				'created_at' => $task->created_at,
				'updated_at' => $task->updated_at,
			]);
		}

		return $tasks;
	}

	/**
	 * Transfer notifications
	 */
	public function transferNotifications()
	{
        set_time_limit(0);

		$notifications = DB::connection('live')->table('notifications')
					->select('*')->orderBy('notifications.id', 'asc')->get();

		foreach ($notifications as $notification) {
			if ($notification->title != 'Project invitation.') {
				Notification::create([
					'id' => $notification->id,
					'user_id' => $notification->user_id,
					'recipient_id' => $notification->recipient_id,
					'project_id' => $notification->project_id,
					'task_id' => $notification->task_id,
					'title' => $notification->title,
					'message' => $notification->message,
					'icon' => 'mdi-bell-ring-outline',
					'event' => $notification->event,
					'is_read' => $notification->is_read,
					'created_at' => $notification->created_at,
					'updated_at' => $notification->updated_at,
				]);
			}
		}

		return $notifications;
	}

	/**
	 * Transfer logs
	 */
	public function transferLogs()
	{
        set_time_limit(0);

		$logs = DB::connection('live')->table('logs')
					->select('*')->orderBy('logs.id', 'asc')->get();

		foreach ($logs as $log) {
			Log::create([
				'id' => $log->id,
				'user_id' => $log->user_id,
				'project_id' => $log->project_id,
				'task_id' => $log->task_id,
				'model' => $log->model,
				'model_id' => $log->model_id,
				'title' => $log->title,
				'message' => $log->message,
				'icon' => 'mdi-clock-outline',
				'event' => $log->event,
				'created_at' => $log->created_at,
				'updated_at' => $log->updated_at,
			]);
		}

		return $logs;
	}

	/**
	 * Transfer attachments
	 */
	public function transferAttachments()
	{
		ini_set('memory_limit', '5000M');
        set_time_limit(0);

		$attachments = DB::connection('live')->table('attachments')
					->select('*')->orderBy('attachments.id', 'asc')->get();

		foreach ($attachments as $attachment) {
			Attachment::create((array) $attachment);

			// create thumbnail for image attachment
			if (in_array($attachment->extension, ['jpg', 'jpeg', 'png']) && !in_array($attachment->id, [2341, 9827])) {
				$http = Http::get('http://146.88.68.206:2040/storage/' . $attachment->path);
				if ($http->handlerStats()['http_code'] == 200) {
					$file = file_get_contents('http://146.88.68.206:2040/storage/' . $attachment->path);
					$directory = explode('/', $attachment->path);
					array_pop($directory);
					$directory = '/public/thumbnail_' . implode('/',$directory);
					Storage::makeDirectory($directory);

					$img = Image::make($file)->resize(null, 80, function ($constraint) {
						$constraint->aspectRatio();
					});
					$img->save(storage_path('app') . $directory . '/' . $attachment->name);
				}
			}
		}

		return $attachments;
	}

	/**
	 * Transfer checklists
	 */
	public function transferChecklists()
	{
        set_time_limit(0);

		$checklists = DB::connection('live')->table('checklists')
					->select('*')->orderBy('checklists.id', 'asc')->get();

		foreach ($checklists as $checklist) {
			Checklist::create((array) $checklist);
		}

		return $checklists;
	}

	/**
	 * Transfer comments
	 */
	public function transferComments()
	{
        set_time_limit(0);

		$comments = DB::connection('live')->table('comments')
					->select('*')->orderBy('comments.id', 'asc')->get();

		foreach ($comments as $comment) {
			$comment->message = preg_replace('/<img[^>]+\>/i', ' ', $comment->message);
			Comment::create((array) $comment);
		}

		return $comments;
	}

	/**
	 * Transfer assignments
	 */
	public function transferAssignments()
	{
        set_time_limit(0);

		$assignments = DB::connection('live')->table('assignments')
					->select('*')->orderBy('assignments.id', 'asc')->get();

		foreach ($assignments as $assignment) {
			Assignment::create((array) $assignment);
		}

		return $assignments;
	}

	/**
	 * Transfer watchers
	 */
	public function transferWatchers()
	{
        set_time_limit(0);

		$watchers = DB::connection('live')->table('watchers')
					->select('*')->orderBy('watchers.id', 'asc')->get();

		foreach ($watchers as $watcher) {
			Watcher::create((array) $watcher);
		}

		return $watchers;
	}

	/**
	 * Transfer task_tags
	 */
	public function transferTaskTags()
	{
		$task_tags = DB::connection('live')->table('task_tag')
					->select('*')->orderBy('task_tag.id', 'asc')->get();

		foreach ($task_tags as $task_tag) {
			TaskTag::create((array) $task_tag);
		}

		return $task_tags;
	}

	/**
	 * Update task visibility
	 */
	public function updateTaskVisibility() {
		Task::whereHas('section', function($query) {
					$query->where('status', 'trashed');
				})->whereHas('project', function($query) {
					$query->whereIn('status', ['trashed', 'archived']);
				})->update(['visible' => false]);
	}

	/**
	 * Insert company records
	 */
	public function insertCompanies() {
		$companies = [
						[
							'package_id' => '3',
							'name' => 'RS Realty Concepts Developer Inc.',
							'slug' => 'rs-realty-ph',
						],
						[
							'package_id' => '3',
							'name' => 'MNLeistung Inc.',
							'slug' => 'mnleistung-de',
						],
						[
							'package_id' => '3',
							'name' => 'Sheirman Construction Consolidated Inc.',
							'slug' => 'scheirman-ph',
						],
						[
							'package_id' => '1',
							'name' => 'Travellers Insurance & Surety Corp.',
							'slug' => 'travellers-insurance-ph',
						]
					];

		foreach ($companies as $company) {
			Company::create($company);
		}
	}
}
