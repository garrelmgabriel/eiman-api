<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Task;
use App\Models\User;
use App\Models\Comment;
use App\Traits\MailTrait;
use App\Events\CommentEvent;
use Illuminate\Http\Request;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
	use NotificationTrait, MailTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'message' => 'required|string'
        ]);

        $replyTo = null;
		$repliedComment = null;

        try {
            DB::beginTransaction();

			$task = Task::find(request('task-id'));

			if (request('reply-to')) {
				$repliedComment = Comment::find(request('reply-to'));

				$replyTo = $repliedComment->reply_to ? $repliedComment->reply_to : $repliedComment->id;

				$this->sendNotification([
					'user_id' => auth()->user()->id,
					'project_id' => $task->project_id,
					'task_id' => $task->id,
					'recipient_id' => $repliedComment->user_id,
					'title' => 'Replied to a comment.',
					'message' => 'replied to your comment on <strong>' . $task->title . '</strong> task.',
					'icon' => 'mdi-comment-plus-outline',
					'event' => 'info'
				]);
			}

			$comment = Comment::create([
				'user_id' => auth()->user()->id,
				'task_id' => request('task-id'),
				'reply_to' => $replyTo,
				'message' => request('message'),
			]);
			$comment['user'] = User::find($comment->user_id);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'Comment',
				'model_id' => $comment->id,
				'title' => 'Added a comment.',
				'message' => 'commented <strong>"$model"</strong> on <strong>$task_name</strong> task.',
				'icon' => 'mdi-comment-plus-outline',
				'event' => 'info'
			];

			// Log activity -- added a comment
			Log::create($activityRecord);

			// Send notification -- added a comment
			$activityRecord['message'] = 'commented on <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group', $repliedComment ? [$repliedComment->user_id] : []);
			broadcast(new CommentEvent('add-comment', $task->id, $comment->id))->toOthers();

			if (request('mentions')) {
				$mentions = array_unique(request('mentions'));

				foreach ($mentions as $user_id) {
					if ($repliedComment && $repliedComment->user_id == $user_id) {
						continue;
					}

					$user = User::find($user_id);

					// send notification
					$activityRecord['recipient_id'] = $user_id;
					$activityRecord['title'] = 'Mentioned in Comment.';
					$activityRecord['message'] = 'mentioned you in a comment on <strong>' . $task->title . '</strong> task.';
					$activityRecord['icon'] = 'mdi-at';
					$this->sendNotification($activityRecord);

					// send email
					if ($user->settings->emails_for_comments_or_mentions) {
						$data = array(
							'project_token' => $task->project->token,
							'task_token' => $task->token,
							'message' => '<strong style="text-transform: capitalize;">' . auth()->user()->display_name . '</strong> mentioned you in a comment on <strong>"' . $task->title . '"</strong> task.',
							'comment' => request('message'),
							'email' => $user->email,
							'subject' => 'Mentioned you in a comment'
						);

						$this->sendMail('emails.comment', $data);
					}
				}
			}

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Comment Submitted Successfully!', 'data' => $comment];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $validated = $request->validate([
            'message' => 'required|string'
        ]);

        Comment::where('id', request('id'))->update([
            'message' => request('message'),
            'is_edited' => 1
        ]);

		$comment = Comment::with('task')->find(request('id'));

		broadcast(new CommentEvent('update-comment', $comment->task->id, $comment->id))->toOthers();

        return ['status' => 'success', 'message' => 'Comment Updated Successfully!', 'data' => $request->all()];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        try {
            DB::beginTransaction();

			$task = Comment::with('task')->find(request('id'))->task;
			broadcast(new CommentEvent('remove-comment', $task->id, request('id')))->toOthers();

			Comment::destroy(request('id'));

			Comment::where('reply_to', request('id'))->update(['reply_to' => null]);

			// Log activity -- removed a comment
			Log::create([
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'title' => 'Removed a comment.',
				'message' => 'removed a comment from <strong>$task_name</strong> task.',
				'icon' => 'mdi-comment-remove-outline',
				'event' => 'danger'
			]);

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Comment Removed Successfully!'];
    }
}
