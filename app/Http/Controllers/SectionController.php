<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Task;
use App\Models\Project;
use App\Models\Section;
use App\Traits\LogTrait;
use App\Events\SectionEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SectionController extends Controller
{
	use LogTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Section::where(['project_id' => request('project-id'), 'status' => request('section-status')])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'section-name' => 'required|string|max:50',
        ]);

        try {
            DB::beginTransaction();

			$sequence = Section::where('project_id', request('project-id'))->orderBy('sequence', 'DESC')->limit(1)->value('sequence');

			$sequence = $sequence ? $sequence : 0;

			$section = Section::create([
				'project_id' => request('project-id'),
				'name' => request('section-name'),
				'color' => request('section-color'),
				'sequence' => $sequence + 1
			]);

			$this->createSectionLog(request('project-id'), $section->id);

			broadcast(new SectionEvent('create',  $section->id))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Section Created Successfully!', 'data' => $section];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Display the statistics of the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStatistics()
    {
		$statistics = [
			'open' => 0,
			'completed' => 0,
			'overdue' => 0,
			'total' => 0,
			'open_percent' => 0,
			'completed_percent' => 0,
			'overdue_percent' => 0,
		];

		$section = Section::find(request('id'));

		$statistics['open'] = $section->openTasks->count();
		$statistics['completed'] = $section->completedTasks->count();
		$statistics['overdue'] = $section->overdueTasks->count();
		$statistics['total'] = $statistics['open'] + $statistics['completed'] + $statistics['overdue'];
		if ($statistics['total']) {
			$statistics['open_percent'] = round(($statistics['open'] / $statistics['total']) * 100, 2);
			$statistics['completed_percent'] = round(($statistics['completed'] / $statistics['total']) * 100, 2);
			$statistics['overdue_percent'] = round(($statistics['overdue'] / $statistics['total']) * 100, 2);
		}

		return $statistics;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        Section::where('id', request('section-id'))
                ->update([
                    'name' => request('section-name'),
                    'color' => request('section-color'),
                    'description' => request('section-description'),
                ]);

		broadcast(new SectionEvent('update-info',  request('section-id')))->toOthers();

        return ['status' => 'success', 'message' => 'Section Updated Successfully!', 'data' => $request->all()];
    }

    /**
     * Update the status of specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStatus()
    {
        try {
            DB::beginTransaction();

			Section::where('id', request('id'))->update(['status' => request('status')]);
			if (request('status') == 'active') {
				Task::where('section_id', request('id'))->update(['visible' => true]);
			} else {
				Task::where('section_id', request('id'))->update(['visible' => false]);
			}

			$section = Section::find(request('id'));
			$projectStatistics = Project::find($section->project_id)->statistics;
			$this->updateSectionStatusLog($section->project_id, request('id'), request('status'));

			broadcast(new SectionEvent('update-status',  request('id')))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Section status was updated successfully!', 'projectStatistics' => $projectStatistics];
    }

    /**
     * Update the sequence of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateSequence(Request $request)
    {
        // Section IDs
        $CSID = request('current-sequence-id');
        $BSID = request('before-sequence-id');
        $ASID = request('after-sequence-id');

        // Get the current sequence of respective IDs
        $BS = $BSID ? Section::where('id', $BSID)->value('sequence') : null; // Sequence of the section before the specified resource
        $AS = $ASID ? Section::where('id', $ASID)->value('sequence') : null; // Sequence of the section after the specified resource
        $NS = null; // new sequence to be assigned to the specified resource

        if ($AS && $BS) { // if the specified resource fits in between of the two concurrent sections
            $NS = ($AS + $BS) / 2;

            if ($AS == $BS) { // if the two concurrent sections have the same sequence, just add 0.2 to the sequence of the specified resource
                $NS = $AS + 0.2;
            }
        } else if (!$AS && $BS) { // if the specified resource was moved at the very least of the collection
            $NS = $BS + 1;
        } else if ($AS && !$BS) { // if the specified resource was moved at the very beggining of the collection
            $NS = $AS - 1;
        }

        if ($AS && ($NS > $AS)) { // if the new sequence of the specified resource is greater than the section after it then sort it to fit the logic by adding 0.2 greater than the specified resource
            $sections = Section::where(['sequence' => $AS, 'project_id' => request('project-id')])->where('id', '>=', $ASID)->get();

            if ($sections) {
                $inc = 0.2;
                foreach ($sections as $section) {
                    $sequence = $NS + $inc;

                    Section::where('id', $section->id)->update(['sequence' => $sequence]);
                    $inc += 0.2;
                }
            }
        }

        Section::where('id', $CSID)->update(['sequence' => $NS]);

		broadcast(new SectionEvent('update-sequence',  $CSID))->toOthers();

        return ['status' => 'success', 'message' => 'Section sequence was updated successfully!'];
    }

    /**
     * Update the status of all the tasks that belongs to specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateTasksStatus()
    {
		$logIcon = 'mdi-redo-variant';

        try {
            DB::beginTransaction();

			$section = Section::find(request('id'));
			Task::where('section_id', request('id'))->update(['status' => request('status')]);
			$projectStatistics = Project::find($section->project_id)->statistics;

			if (request('status') == 'trashed' || request('status') == 'archived') {
				$logIcon = request('status') == 'trashed' ? 'mdi-trash-can-outline' : 'mdi-archive-outline';
			}

			// Log activity -- update status of all section's tasks
			Log::create([
				'user_id' => auth()->user()->id,
				'project_id' => $section->project_id,
				'model' => 'Section',
				'model_id' => request('id'),
				'title' => 'Updated status of all sections\' tasks.',
				'message' => (request('status') == 'open' ? 'reopened' : request('status')) . ' all tasks under <strong>$model</strong> section.',
				'icon' => $logIcon,
				'event' => (request('status') == 'open' ? 'info' : 'danger')
			]);

			broadcast(new SectionEvent('update-tasks',  request('id')))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Section\'s tasks status were updated successfully!', 'projectStatistics' => $projectStatistics];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        //
    }
}
