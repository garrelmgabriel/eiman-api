<?php

namespace App\Http\Controllers;

use App\Models\ProjectGroup;
use Illuminate\Http\Request;

class ProjectGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectGroup  $ProjectGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectGroup $ProjectGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectGroup  $ProjectGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectGroup $ProjectGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectGroup  $ProjectGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectGroup $ProjectGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectGroup  $ProjectGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectGroup $ProjectGroup)
    {
        //
    }
}
