<?php

namespace App\Http\Controllers;

use App\Models\ProjectSetting;
use Illuminate\Http\Request;

class ProjectSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectSetting  $ProjectSetting
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectSetting $ProjectSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectSetting  $ProjectSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectSetting $ProjectSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectSetting  $ProjectSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectSetting $ProjectSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectSetting  $ProjectSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectSetting $ProjectSetting)
    {
        //
    }
}
