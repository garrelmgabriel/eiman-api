<?php

namespace App\Http\Controllers;

use Image;
use Carbon\Carbon;
use App\Models\User;
use App\Traits\TaskTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    use TaskTrait;

    /**
     * Display email listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmails()
    {
		$term = request('term');
		$company_id = auth()->user()->company_id;

        return User::select('firstname', 'lastname', 'email')
					->where('id', '<>', auth()->user()->id)
					->where(function($query) use ($term) {
						$query->where('email', 'LIKE', '%' . $term . '%')
								->orWhere('firstname', 'LIKE', '%' . $term . '%')
								->orWhere('lastname', 'LIKE', '%' . $term . '%');
					})
					->when($company_id != 2, function ($query) use ($company_id) {
						$query->whereIn('company_id', [2, $company_id]);
					})
                    ->where('status', 'active')
                    ->whereNull('deleted_at')
                    ->limit(5)->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
		if (request()->ajax()) {
			$projectId = request('project-id');

			return User::when($projectId, function($query) use ($projectId) {
					$query->with(['membership' => function($query) use ($projectId) {
						$query->where('project_id', request('project-id'));
					}]);
				})->when(!$projectId, function($query) {
					$query->with(['projects' => function($query) {
						$query->with('team')->where('user_level', 1)->where('status', 'active');
					}]);
				})->find(request('id'));
		}

		return view('account');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
		$fields = request()->all();
		$user = auth()->user();

		if (array_key_exists('password', $fields)) { // update password
            $validated = $request->validate([
                'old-password' => [
                    'required',
                    function ($attribute, $value, $fail) use ($user) {
                        if (!Hash::check($value, auth()->user()->password)) {
                            $fail('Your password was not updated, since the provided old password does not match.');
                        }
                    }
                ],
                'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/|string|min:6|confirmed',
            ],
            [
                'password.regex' => 'Password must contain numbers and UPPERCASE/lowercase letters.',
            ]);

			$fields = [ 'password'   => Hash::make($fields['password']) ];
		} else if (array_key_exists('firstname', $fields)) { // update basic info
			$validated = $request->validate([
                'firstname' => ['required', 'regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
                'middlename' => ['nullable','regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
                'lastname' => ['required', 'regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
                'nickname' => ['nullable','regex:/^[ÑñA-Za-z\s\-]+$/', 'string', 'max:50'],
                'title' => ['nullable','regex:/^[ÑñA-Za-z\s\-\.]+$/', 'string', 'max:50'],
                'email' => ['required', 'string', 'regex:/(.*)gmail\.com|scheirman\.ph|rsrealty\.ph|mnleistung\.de|travellersinsuranceph\.com|gmx\.net$/i', 'max:255', 'unique:users,email,' . auth()->user()->id],
                'verify-password' => ['nullable',
                                        function ($attribute, $value, $fail) use ($user) {
                                            if (!Hash::check($value, $user->password)) {
                                                $fail('Incorrect Password.');
                                            }
                                        }
                                    ]
            ],
            [
                'email.regex' => 'We appreciate your interest on using our system. However, at the moment we offer this service only to specific companies!',
            ]);

			unset($fields['edit-email']);
			unset($fields['verify-password']);
		}

		User::where('id', $user->id)->update($fields);
		return User::find($user->id);
    }

    /**
     * Update the specified resource in storage via admin panel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function updateViaAdmin(Request $request, User $user)
    {
        if (auth()->user()->user_level == 'admin' && User::find(request('user-id'))->user_level == 'superadmin') {
            return response()->json(['errors' => 'You don\'t have permission to perform this action.'], 403);
        }

		User::where('id', request('user-id'))->update([request('key') => (request('key') == 'email_verified_at' ? Carbon::now()->format('Y-m-d H:i:s') : request('value'))]);
    }

    /**
     * Update avatar of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function updateAvatar(Request $request, User $user)
    {
		if ($request->hasFile('avatar')) {
			$fileName = 'avatar_' . md5(mt_rand(1, 20)) . date('Ymdgi');
            $file = $request->file('avatar');
            if ($file->getSize() > 5242880) {
                return response()->json(['errors' => 'File size too large.'], 413);
            }
            $ext = $file->extension();
            $name = $fileName . '.' . $ext;
            $directory = 'public/avatar/user_' . auth()->user()->id;

            Storage::disk('local')->deleteDirectory($directory);
            Storage::makeDirectory($directory);

            $img = Image::make($file->getRealPath());
            $img->fit(170, 170, function ($contraint) {}, 'top')->save(storage_path('app') . '/' . $directory . '/' . $name);

			User::where('id', auth()->user()->id) ->update([ 'avatar' => '/storage/avatar/user_' .  auth()->user()->id . '/' . $name ]);

			return ('/storage/avatar/user_' .  auth()->user()->id . '/' . $name);
        } else {
			User::where('id', auth()->user()->id) ->update([ 'avatar' => request('avatar') ]);

			return request('avatar');
		}
    }

    /**
     * Remove avatar of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function removeAvatar(Request $request, User $user)
    {
		$directory = 'public/avatar/user_' . auth()->user()->id;

        Storage::disk('local')->deleteDirectory($directory);

        User::where('id', auth()->user()->id)->update([ 'avatar' => null ]);

		return User::find($user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAssignedProjectTasks()
    {
		$projectId = request('project-id');

        $tasks = User::with(['assigned' => function($query) use ($projectId) {
                    $query->whereNotIn('status', ['archived', 'trashed'])
							->where('project_id', $projectId);;
                }])->find(request('user-id'))->assigned;

		foreach ($tasks as $key => $task) {
			$tasks[$key] = $this->parseBasicTask($task);
		}

        return $tasks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function getAssignedCalendarTasks()
	{
		$selectors = request()->all();

		$tasks = User::with(['assigned' => function($query) use ($selectors) {
					$query->whereNotIn('status', ['archived', 'trashed'])
							->where(function($query) use ($selectors) {
								$start = Carbon::create($selectors['start-date'])->format('Y-m-d H:i:s');
								$end = Carbon::create($selectors['end-date'])->format('Y-m-d H:i:s');

								$query->whereBetween('start_date', [$start, $end])
									->orWhereBetween('due_date', [$start, $end])
									->orWhereBetween('completed_at', [$start, $end])
									->orWhere(function($query) use ($start, $end) {
										$query->where('start_date', '<=', $start)->where('due_date', '>=', $end);
									});
							});
				}])->find(auth()->user()->id)->assigned;

		foreach ($tasks as $key => $task) {
			$tasks[$key] = $this->parseBasicTask($task);
		}

		return $tasks;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function getAssignedUnscheduledTasks()
	{
		$tasks = User::with(['assigned' => function($query) {
					$query->whereNotIn('status', ['archived', 'trashed'])
						->whereNull('start_date')->whereNull('due_date');
				}])->find(auth()->user()->id)->assigned;

		foreach ($tasks as $key => $task) {
			$tasks[$key] = $this->parseBasicTask($task);
		}

		return $tasks;
	}
}
