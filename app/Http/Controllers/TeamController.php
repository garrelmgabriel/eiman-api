<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Log;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Project;
use App\Models\Watcher;
use App\Events\TeamEvent;
use App\Models\Assignment;
use Illuminate\Http\Request;
use App\Models\AnnouncementUser;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
	use NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Team $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
		/**
		 * Update team member's user level
		 *
		 * 0 - Member
		 * 1 - Owner
		 * 2 - Admin
		 * 3 - Viewer
		 */

		$requests = request()->all();

        try {
            DB::beginTransaction();

			if ($request['user-role'] == '3' || $request['user-role'] == '4') {
				// remove as assigned user from all tasks then log
				$assigned = User::with(['assigned' => function($query) use ($requests) {
								$query->where('project_id', $requests['project-id']);
							}])->find($requests['user-id'])->assigned;

				foreach ($assigned as $a) {
					Assignment::destroy($a->pivot->id);
				}
			}

			Team::where(['project_id' => $requests['project-id'], 'user_id' => $requests['user-id']])
				->update([
					'user_level' => $request['user-role'],
					'duration' => $request['role-duration'] ? Carbon::create($request['role-duration'])->format('Y-m-d H:i:s') : null
				]);

			broadcast(new TeamEvent('update-level', $requests['user-id'], $requests['project-id']))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

		return Team::where(['project_id' => $requests['project-id'], 'user_id' => $requests['user-id']])->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Team $team
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request, Team $team)
    {
		$owner = isset($request->owner) ? $request->owner : auth()->user()->id;
		$newOwner = request('user-id');
        try {
            DB::beginTransaction();

			// Set a new project owner
			if (request('user-id') == 'current') {
				$team = Team::where('user_id', auth()->user()->id)->latest()->first();

				$sequence = $team ? $team->sequence : 0;

				Team::create([
					'user_id' => auth()->user()->id,
					'project_id' => request('project-id'),
					'user_level' => 1,
					'sequence' => $sequence + 1
				]);

				$newOwner = auth()->user()->id;
			} else {
				Team::where(['project_id' => request('project-id'), 'user_id' => $newOwner])->update(['user_level' => 1]);
			}

			// Remove current user as an owner and set as an owner
			Team::where(['project_id' => request('project-id'), 'user_id' => $owner])->update(['user_level' => 2]);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => request('project-id'),
				'title' => 'Transferred ownership.',
				'model' => 'user',
				'model_id' => $newOwner,
				'message' => 'set <strong class="text-capitalize">$model</strong> as the project owner.',
				'icon' => 'mdi-account-star-outline',
				'event' => 'purple'
			];

			// Log activity -- remove announcement
			Log::create($activityRecord);

			// Send notification -- transfer ownership
			$activityRecord['recipient_id'] = $newOwner;
			$activityRecord['message'] = 'set you as the project owner.';
			$this->sendNotification($activityRecord);

			broadcast(new TeamEvent('update-level', $newOwner, request('project-id')))->toOthers();

			// broadcast change to the owner if transferring of project ownership was made by the admin
			if (isset($request->owner)) {
				$this->sendNotification([
					'user_id' => auth()->user()->id,
					'project_id' => request('project-id'),
					'recipient_id' => $owner,
					'title' => 'Transferred ownership.',
					'message' => 'removed you as the project owner.',
					'icon' => 'mdi-account-star-outline',
					'event' => 'purple'
				]);

				broadcast(new TeamEvent('update-level', $owner, request('project-id')))->toOthers();
			}

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
		$requests = request()->all();

		// set log messages
        $wMsg = 'removed <strong class="text-capitalize">$model</strong> from this project and was automatically unassigned as a watcher from <strong>$task_name</strong> task.';
        $aMsg = 'removed <strong class="text-capitalize">$model</strong> from this project and was automatically unassigned from <strong>$task_name</strong> task.';
        $lTitle = 'Removed a member.';
		$lMsg = 'removed <strong class="text-capitalize">$model</strong> from this project.';
		$lIcon = 'mdi-account-remove-outline';

        if ($requests['user-id'] == auth()->user()->id) {
            $wMsg = 'left this project and was automatically unassigned as a watcher from <strong>$task_name</strong> task.';
            $aMsg = 'left this project and was automatically unassigned from <strong>$task_name</strong> task.';
			$lTitle = 'Left the project.';
            $lMsg = 'left this project.';
			$lIcon = 'mdi-account-arrow-right-outline';
        }

        try {
            DB::beginTransaction();

			// remove as watcher from all tasks then log
			$watched = User::with(['watched' => function($query) use ($requests) {
							$query->where('project_id', $requests['project-id']);
						}])->find($requests['user-id'])->watched;

			foreach ($watched as $w) {
				Log::create([
					'user_id' => auth()->user()->id,
					'project_id' => $requests['project-id'],
					'task_id' => $w->id,
					'model' => 'User',
					'model_id' => $requests['user-id'],
					'title' => 'Unassigned a watcher.',
					'message' => $wMsg,
					'icon' => 'mdi-eye-remove-outline',
					'event' => 'danger'
				]);

				Watcher::destroy($w->pivot->id);
			}

			// remove as assigned user from all tasks then log
			$assigned = User::with(['assigned' => function($query) use ($requests) {
							$query->where('project_id', $requests['project-id']);
						}])->find($requests['user-id'])->assigned;

			foreach ($assigned as $a) {
				Log::create([
					'user_id' => auth()->user()->id,
					'project_id' => $requests['project-id'],
					'task_id' => $a->id,
					'model' => 'User',
					'model_id' => $requests['user-id'],
					'title' => 'Unassigned a user.',
					'message' => $aMsg,
					'icon' => 'mdi-account-remove-outline',
					'event' => 'danger'
				]);

				Assignment::destroy($a->pivot->id);
			}

			// remove as member from project then log and notify
			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $requests['project-id'],
				'model' => 'User',
				'model_id' => $requests['user-id'],
				'title' => $lTitle,
				'message' => $lMsg,
				'icon' => $lIcon,
				'event' => 'danger'
			];

			// Log activity -- remove project member
			Log::create($activityRecord);

			// Send notification -- remove project member
			$project = Project::with('announcement')->find($requests['project-id']);
			$activityRecord['message'] = 'removed you from this project.';
			$activityRecord['recipient_id'] = $requests['user-id'];
			$this->sendNotification($activityRecord);

			// remove user from meeting attendees if exists
			if ($project->announcement) {
				AnnouncementUser::where(['announcement_id' => $project->announcement->id, 'user_id' => $requests['user-id']])->delete();
			}

			Team::where(['project_id' => $requests['project-id'], 'user_id' => $requests['user-id']])->delete();

			broadcast(new TeamEvent('remove-member', $requests['user-id'], $requests['project-id']))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }
    }
}
