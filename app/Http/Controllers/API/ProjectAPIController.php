<?php


namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller as Controller;
use App\Models\Task;
use App\Traits\TaskTrait;
use App\Traits\TeamTrait;
use App\Models\Project;
use App\Models\User;
use Auth;
use App\Models\DashboardSetting;
class ProjectAPIController extends Controller
{

    use TeamTrait, TaskTrait;


    public function getProjects()
    {

        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
       

        $projects = null;
        $category = DashboardSetting::where('user_id', auth()->user()->id)->first()->project_display;

		$projects = User::with(['projects' => function($query) use ($category) {
            if ($category == 'owned') {
                $query->where('user_level', 1);
            }

            if ($category == 'favorite') {
                $query->where('is_favorite', 1);
            }
			$query->where('status', 'active')->orderBy('teams.created_at', 'DESC')->limit(10);
        }])->find(auth()->user()->id)->projects;

        return $projects;
    }

    public function getBoard()
    {
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        
        $sections = Project::with(['sections' => function($query) {
                        $query->where('status', request('section_status'))
                                ->orderBy('sections.sequence', 'ASC');
                    }])->find(request('project_id'))->sections;

        foreach ($sections as $section) {
            $section['tasks'] = $this->getSectionTasks($section->id, request('filters'));
        }

        return $sections;
    }


    public function getProjStat()
    {
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}

        $statistics = [];

        // total project count
        $statistics['total_projects'] = User::with('projects')->find(auth()->user()->id)->projects->count();

        // total active project count
        $statistics['active_projects'] = User::with(['projects' => function($query) {
            $query->where('status', 'active');
        }])->find(auth()->user()->id)->projects->count();

        // total archived project count
        $statistics['archived_projects'] = User::with(['projects' => function($query) {
            $query->where('status', 'archived');
        }])->find(auth()->user()->id)->projects->count();

        // total owned project count
        $statistics['owned_projects'] = User::with(['projects' => function($query) {
            $query->where('user_level', 1);
        }])->find(auth()->user()->id)->projects->count();

        return $statistics;
    }

    public function getAllProjects(){
        
       if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        
        $status = request('status');
        $name = request('name');

        $projects = User::with(['projects' => function($query) use ($status, $name) {
			if ($name) {
				$query->where('name', 'LIKE', '%' . $name . '%');
			}

            if ($status == 'active') {
                $query->where('status', 'active');
            } else if ($status == 'archived') {
                $query->where('status', 'archived');
            } else if ($status == 'trashed') {
                $query->where('status', 'trashed');
            } else if ($status == 'owned') {
                $query->where('user_level', 1);
            } else if ($status == 'allowed') {
                $query->where('status', 'active')->whereIn('user_level', [0, 1, 2]);
            }

			$query->orderby('is_favorite', 'DESC');
        }])->find(auth()->user()->id)->projects;

        return $projects;
    }

}    