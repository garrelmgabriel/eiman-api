<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Log;

class LogAPIController extends Controller
{
    public function index()
    {
        $filters = request()->all();

        return Log::when($filters['log_filter'], function ($query) use ($filters) {
            $query->where('logs.title', 'LIKE', '%' . $filters['log_filter'] . '%');
        })->when(array_key_exists('project_id', $filters), function ($query) use ($filters) { // project logs
            $query->where('project_id', $filters['project_id']);
        })->when(array_key_exists('task_id', $filters), function ($query) use ($filters) { // task logs
            $query->where('task_id', $filters['task_id']);
        })->when(array_key_exists('user_id', $filters), function ($query) use ($filters) { // user logs
            $query->where('user_id', $filters['user_id']);
        })
            ->orderBy('created_at', 'DESC')->paginate(15);
    }
}