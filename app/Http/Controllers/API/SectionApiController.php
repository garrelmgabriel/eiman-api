<?php


namespace App\Http\Controllers\API;
use App\Models\Section;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Events\SectionEvent;

class SectionApiController extends Controller
{


    public function updateSequence(Request $request)
    {
        // Section IDs
        $CSID = request('current_sequence_id');
        $BSID = request('before_sequence_id');
        $ASID = request('after_sequence_id');

        // Get the current sequence of respective IDs
        $BS = $BSID ? Section::where('id', $BSID)->value('sequence') : null; // Sequence of the section before the specified resource
        $AS = $ASID ? Section::where('id', $ASID)->value('sequence') : null; // Sequence of the section after the specified resource
        $NS = null; // new sequence to be assigned to the specified resource

        if ($AS && $BS) { // if the specified resource fits in between of the two concurrent sections
            $NS = ($AS + $BS) / 2;

            if ($AS == $BS) { // if the two concurrent sections have the same sequence, just add 0.2 to the sequence of the specified resource
                $NS = $AS + 0.2;
            }
        } else if (!$AS && $BS) { // if the specified resource was moved at the very least of the collection
            $NS = $BS + 1;
        } else if ($AS && !$BS) { // if the specified resource was moved at the very beggining of the collection
            $NS = $AS - 1;
        }

        if ($AS && ($NS > $AS)) { // if the new sequence of the specified resource is greater than the section after it then sort it to fit the logic by adding 0.2 greater than the specified resource
            $sections = Section::where(['sequence' => $AS, 'project_id' => request('project_id')])->where('id', '>=', $ASID)->get();

            if ($sections) {
                $inc = 0.2;
                foreach ($sections as $section) {
                    $sequence = $NS + $inc;

                    Section::where('id', $section->id)->update(['sequence' => $sequence]);
                    $inc += 0.2;
                }
            }
        }

        Section::where('id', $CSID)->update(['sequence' => $NS]);

		broadcast(new SectionEvent('update-sequence',  $CSID))->toOthers();

        return ['status' => 'success', 'message' => 'Section sequence was updated successfully!'];
    }



}