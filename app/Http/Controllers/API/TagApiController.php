<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Project;
use App\Traits\TagTrait;

class TagApiController extends Controller
{
    public function getDetails()
    {
        return Tag::where('project_id', request('project_id'))->get();
    }
}
