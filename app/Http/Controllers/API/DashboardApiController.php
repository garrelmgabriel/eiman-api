<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DashboardSetting;
use Carbon\Carbon;
use App\Http\Controllers\API\BaseController as BaseController;
use Auth;

class DashboardApiController extends BaseController
{
    public function getDashboardStatistics()
    {
        
		if(request('user_id')){Auth::loginUsingId(request('user_id'));}

        $counts = User::with(['projects' => function($query) {	
                        $query->where('status', 'active');
                    }])->find(auth()->user()->id)->projects->count();

        if ($counts) {
			$statCategories = auth()->user()->dashboardSettings->statistics;

            // total projects count
			if (in_array('stat_total_projects', $statCategories)) {
				$tcount = User::with('projects')->find(auth()->user()->id)->projects->count();

				$statistics[] = [
				'name'=>'Total Projects',
				'color'=>'theme',
				'icon'=>'fe-layers',
				'count'=>$tcount
				];
			}

			// total owned projects count
			if (in_array('stat_owned_projects', $statCategories)) {

				$tcount = User::with(['projects' => function($query) {
					$query->where('user_level', 1);
				}])->find(auth()->user()->id)->projects->count();

				$statistics[] = [
				'name'=>'Owned Projects',
				'color'=>'accent',
				'icon'=>'fe-star',
				'count'=>$tcount
				];
			}

			// total active projects count
			if (in_array('stat_active_projects', $statCategories)) {
				$tcount = User::with(['projects' => function($query) {
					$query->where('status', 'active');
				}])->find(auth()->user()->id)->projects->count();

				$statistics[] = [
				'name'=>'Active Projects',
				'color'=>'primary',
				'icon'=>'fe-activity',
				'count'=>$tcount
				];
			}

			// total project groups count -- TODO
			if (in_array('stat_total_groups', $statCategories)) {
				$statistics[] = [
				'name'=>'Project Groups',
				'color'=>'info',
				'icon'=>'fe-box',
				'count'=>0
				];
			}

			// total archived projects count
			if (in_array('stat_archived_projects', $statCategories)) {
				$tcount = User::with(['projects' => function($query) {
					$query->where('status', 'archived');
				}])->find(auth()->user()->id)->projects->count();
			

				$statistics[] = [
				'name'=>'Archived Projects',
				'color'=>'secondary',
				'icon'=>'fe-archive',
				'count'=>tcount
				];
			}

			// total tasks today count
			if (in_array('stat_tasks_today', $statCategories)) {
				$tcount = User::with(['assigned' => function($query) {
					$query->whereDate('tasks.start_date', Carbon::today());
				}])->find(auth()->user()->id)->assigned->count();
				$statistics[] = [
				'name'=>'Task Today',
				'color'=>'warning',
				'icon'=>'fe-airplay',
				'count'=>$tcount
				];
			}

            // total tasks assigned count
			if (in_array('stat_total_tasks', $statCategories)) {
				$tcount = User::with('assigned')->find(auth()->user()->id)->assigned->count();
				$statistics[] = [
				'name'=>'Tasks Assigned',
				'color'=>'pink',
				'icon'=>'fe-user',
				'count'=>$tcount
				];
			}

            // total active tasks count
			if (in_array('stat_active_tasks', $statCategories)) {

				$tcount = User::with(['assigned' => function($query) {
					$query->where('tasks.status', 'open');
				}])->find(auth()->user()->id)->assigned->count();

				$statistics[] = [
				'name'=>'Active Tasks',
				'color'=>'purple',
				'icon'=>'fe-list',
				'count'=>$tcount
				];

			}

            // total completed tasks count
			if (in_array('stat_completed_tasks', $statCategories)) {
				$tcount = User::with(['assigned' => function($query) {
					$query->where('tasks.status', 'completed');
				}])->find(auth()->user()->id)->assigned->count();

				$statistics[] = [
				'name'=>'Completed Tasks',
				'color'=>'success',
				'icon'=>'fe-check-circle',
				'count'=>$tcount
				];
			}

			// total overdue tasks count
			if (in_array('stat_overdue_tasks', $statCategories)) {
				$tcount = User::with(['assigned' => function($query) {
					$query->where('tasks.status', 'overdue');
				}])->find(auth()->user()->id)->assigned->count();

				$statistics[] = [
				'name'=>'Overdue Tasks',
				'color'=>'danger',
				'icon'=>'fe-calendar',
				'count'=>$tcount
				];

			}
        }
		return $this->sendResponse(['statistics' => $statistics, 'counts' => $counts], 'Fetch Success');
        // return response()->json(['statistics' => $statistics, 'counts' => $counts]);
    }

	public function updateDash(Request $request, DashboardSetting $dashboardSetting)
    {
		if(request('user_id')){Auth::loginUsingId(request('user_id'));}

		if (request('theme')) {
			DashboardSetting::where('user_id', auth()->user()->id)->update(['theme_color' => request('theme'), 'accent_color' => request('accent')]);
			return ['status' => 'success', 'message' => 'Theme Updated Successfully!'];
		}

		if (request('key') == 'statistics') {
			DashboardSetting::where('user_id', auth()->user()->id)->update([request('key') => json_encode(explode(',', request('value')))]);
			return ['status' => 'success', 'message' => 'Statistics Display Updated Successfully!'];
		}


		if (request('key')) {
			DashboardSetting::where('user_id', auth()->user()->id)->update([request('key') => request('value')]);
			return ['status' => 'success', 'message' => 'Updated Successfully!'];
				}

		
	
    }
}
