<?php


namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller as Controller;
use App\Models\Log;
use App\Models\Task;
use App\Traits\TaskTrait;
use App\Traits\TeamTrait;
use App\Events\TaskEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskApiController extends Controller
{

    use TeamTrait, TaskTrait;

    public function taskdets(Request $request)
    {
     
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        $task = Task::find(request('task_id'));
       
        if (request('parse') == 'basic') {
           
            $task = $this->parseBasicTask($task);
        } else if (request('parse') == 'advance') {
           
            $task = $this->parseAdvanceTask($task);
        }
        // Get the role of the logged in user
        $member = $this->getMembership($task->project_id, auth()->user()->id);

        return ['task' => $task, 'user_id' => $member->user_id, 'user_role' => $member->user_level];
    }

    public function updateSequence(Request $request)
    {

        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
           // Task IDs
           $CSID = request('current_sequence_id');
           $BSID = request('before_sequence_id');
           $ASID = request('after_sequence_id');
   
           // Get the current sequence of respective IDs
           $BS = $BSID ? Task::where('id', $BSID)->value('sequence') : null; // Sequence of the section before the specified resource
           $AS = $ASID ? Task::where('id', $ASID)->value('sequence') : null; // Sequence of the section after the specified resource
           $NS = null; // new sequence to be assigned to the specified resource
   
           if ($AS && $BS) { // if the specified resource fits in between of the two concurrent sections
               $NS = ($AS + $BS) / 2;
   
               if ($AS == $BS) {
                   $NS = $AS + 0.2;
               }
           } else if (!$AS && $BS) { // if the two concurrent sections have the same sequence, just add 0.2 to the sequence of the specified resource
               $NS = $BS + 1;
           } else if ($AS && !$BS) { // if the specified resource was moved at the very least of the collection
               $NS = $AS - 1;
           } else if (!$AS && !$BS) { // if the specified resource was moved at the very beggining of the collection
               $NS = 1;
           }
   
           if ($AS && ($NS > $AS)) { // if the new sequence of the specified resource is greater than the section after it then sort it to fit the logic by adding 0.2 greater than the specified resource
               $tasks = Task::where(['sequence' => $AS, 'section_id' => request('section_id')])->where('id', '>=', $ASID)->get();
   
               if ($tasks) {
                   $inc = 0.2;
                   foreach ($tasks as $task) {
                       $sequence = $NS + $inc;
   
                       Task::where('id', $task->id)->update(['sequence' => $sequence]);
                       $inc += 0.2;
                   }
               }
           }
   
           Task::where('id', $CSID)->update(['sequence' => $NS, 'section_id' => request('section_id')]);
   
           $task = Task::find($CSID);
   
           $activityRecord = [
               'user_id' => auth()->user()->id,
               'project_id' => $task->project_id,
               'task_id' => $task->id,
               'model' => 'Section',
               'model_id' => request('section_id'),
               'title' => 'Moved task.',
               'message' => 'moved <strong>$task_name</strong> task to <strong>$model</strong>.',
               'icon' => 'mdi-cursor-move',
               'event' => 'info'
           ];
   
           // Log activity -- move task
           Log::create($activityRecord);
   
           // Send notification -- move task
           $activityRecord['message'] = 'moved <strong>' . $task->title . '</strong> task.';
           $this->sendNotification($activityRecord, 'all');
           broadcast(new TaskEvent('move-task', $task->id))->toOthers();
   
           return ['status' => 'success', 'message' => 'Task sequence was updated successfully!'];
    }

    

    public function search()
    {
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
		$tasks = [];
		$filters = request()->all();

		$response = Task::whereHas('project', function($query) use ($filters) {
						if ($filters['project_filter'] != 'all') {
							$query->where('projects.id', $filters['project_filter']);
						}
						$query->whereHas('team', function($query) {
							$query->where('users.id', auth()->user()->id);
						});
					})
					->when($filters['search_keyword'], function ($query) use ($filters) {
						$query->where('tasks.title', 'LIKE', '%' . $filters['search_keyword'] . '%');
					})
					->when($filters['status_filter'] != 'all', function ($query) use ($filters) {
						$query->where('tasks.status', $filters['status_filter']);
					})
					->when($filters['tag_filter'] != 'all', function ($query) use ($filters) {
						$query->whereHas('tags', function($query) use ($filters) {
							$query->where('tags.id', $filters['tag_filter']);
						});
					})
					->paginate(10);

		foreach ($response as $key => $task) {
			$response[$key] = $this->parseBasicTask($task);
		}

		return $response;
    }



}    