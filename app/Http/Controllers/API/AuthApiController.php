<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;




class AuthApiController extends BaseController
{
   //AUTHENTICATION FUNCTION//
    function mob_authenticate(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            // $success['token'] =  $user->createToken('MyApp')->plainTextToken; 
            $success['userdata'] =  $user;
   
            return $this->sendResponse($success, 'User login successfully.');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }


    function sampleapi(Request $request)
    {
        return null;
    }

    //GENERATE TOKEN//
    function mob_gen_token($email = null, $password = null, Request $request)
    {
        try {
            // Begin a transaction
            DB::beginTransaction();
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                //condition for the account with token already
                if (Auth::user()->access_token) {
                    Auth::logout();
                    $success['userdata'] =  Auth::user();
                    return $this->sendResponse($success, 'Token already generated.');
                }
                //token generation
                Auth::user()->access_token = $request->user()->createToken('token')->plainTextToken;
                Auth::user()->save();
                DB::commit();

                return response()->json(['status' => 200, 'error' => "Token has been successfuly generated "]);
                Auth::logout();
            }
            return response()->json(['status' => 422, 'error' => "Token has been successfuly generated "]);
        } catch (Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();
            return response()->json(['status' => 422, 'error' => "Token has been successfuly generated "]);
        }
    }

    function getuserdata(Request $request)
    {
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        return Auth::user();
    }

    function getsettings(Request $request)
    {
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        return auth()->user()->settings;

    }


}