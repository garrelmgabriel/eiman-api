<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;

class UserApiController extends Controller
{
    public function getTeam()
    {
        return Project::find(request('project_id'))->team;
    }
}
