<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invitation;
use App\Traits\InvitationTrait;
use Auth;
class InvitationAPIController extends Controller
{
    use InvitationTrait;

    public function getInvite(){
        if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        $status = request('status');

        $new = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->count();
        $invitations = Invitation::with('sender', 'project')->where('recipient_id', auth()->user()->id)
                                ->when($status, function ($query) use ($status) {
                                    return $query->where('status', $status);
                                })->orderBy('created_at', 'DESC')->get();

        return ['new' => $new, 'invitations' => $invitations];
    }
}
