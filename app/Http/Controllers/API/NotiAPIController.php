<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Notification;
use Auth;

class NotiAPIController extends Controller
{
	public function readNotif(Notification $notification)
    {
        return Notification::where('id', request('notif_id'))->update(['is_read' => Carbon::now()->format('Y-m-d H:i:s')]);
    }

    public function getNotif(){
		if(request('user_id')){Auth::loginUsingId(request('user_id'));}
        $filters = request()->all();

        $new = Notification::where(['recipient_id' => auth()->user()->id, 'is_read' => null])
							->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
								$query->where('project_id', $filters['project-id']);
							})->count();
		$notifications = Notification::when($filters['notif_filter'], function ($query) use ($filters) {
								$query->where('title', 'LIKE', '%' . $filters['notif_filter'] . '%');
							})->when($filters['notif_status_filter'], function ($query) use ($filters) {
								if ($filters['notif_status_filter'] == 'unread') {
									$query->whereNull('is_read');
								} else {
									$query->whereNotNull('is_read');
								}
							})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project notifs
								$query->where('project_id', $filters['project-id']);
							})
							->where('recipient_id', auth()->user()->id)
							->orderBy('created_at', 'DESC')->paginate(15);

		return ['new' => $new, 'notifications' => $notifications];
    }
}
