<?php

namespace App\Http\Controllers;

use JavaScript;
use Carbon\Carbon;
use App\Models\Team;
use App\Models\Project;
use App\Traits\TeamTrait;
use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Traits\InvitationTrait;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\Auth;

class InvitationController extends Controller
{
    use InvitationTrait;
    use TeamTrait;
	use NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = request('status');

        $new = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->count();
        $invitations = Invitation::with('sender', 'project')->where('recipient_id', auth()->user()->id)
                                ->when($status, function ($query) use ($status) {
                                    return $query->where('status', $status);
                                })->orderBy('created_at', 'DESC')->get();

        return ['new' => $new, 'invitations' => $invitations];
    }

	/**
	 * Count listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function count()
	{
		return Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->count();
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::find(request('project-id'));

        $this->sendInvitation($project->id, $project->name, request('project-invitation-emails'), request('project-invitation-message'));
    }

    /**
     * Accept invitation via email.
     *
     * @param  $id - Invitation ID
     * @return \Illuminate\Http\Response
     */
    public function acceptEmailInvitation($id)
    {
        $invitation = Invitation::find($id);

        // Check if invitation exists
        if (!$invitation) {
			return view('errors/invitation')->with('data', ['image' => 'invalid-invitation.png', 'title' => 'Invalid invitation', 'message' => 'This project invitation is no longer valid. Please try another one or contact the person who sent you an invitation.']);
        }

        // Check if invitation was already accepted
        if ($invitation->status == 'accepted') {
            $project = Project::find($invitation->project_id);

            return redirect('project/' . $project->token);
        }

        // Check if invitation was already declined
        if ($invitation->status == 'declined') {
			return view('errors/invitation')->with('data', ['image' => 'declined-invitation.png', 'title' => 'Declined invitation', 'message' => 'This project invitation was already declined. Please try another one or contact the person who sent you an invitation.']);
        }

        // Check if invitation was already expired
        if ($invitation->status == 'expired' || Carbon::create($invitation->expiration)->lessThan(now())) {
            if ($invitation->status != 'expired') {
                Invitation::where('id', $id)->update(['status' => 'expired']);
            }

            return view('errors/invitation')->with('data', ['image' => 'expired-invitation.png', 'title' => 'Expired invitation', 'message' => 'This project invitation has already expired last <strong>' . Carbon::create($invitation->expiration)->isoFormat('MMM Do, YYYY') . '</strong>. Please try another one or contact the person who sent you an invitation.']);
        }

        // Check if the invited user has an account
        if ($invitation->recipient_id) {
            // Check if there's a logged in user and if that user is the invited user
            if (Auth::check() && $invitation->recipient_id == auth()->user()->id) {
				if (!Team::where(['user_id' => auth()->user()->id, 'project_id' => $invitation->project_id])->exists()) { // check if already a member of the project
					$this->storeMember($invitation->project_id, $invitation->user_id);
				}

                Invitation::where('id', $id)->update(['status' => 'accepted']);

                $project = Project::find($invitation->project_id);

                JavaScript::put(['JOINED' => true]);

                return redirect('project/' . $project->token);
            }

            // if the invited user has an account but not logged in
            session(['invitation' => $invitation]);
            if (Auth::check()) { Auth::logout(); }
            return redirect('login');
        }

        // if the invited user doesn't have an account
        session(['invitation' => $invitation]);
        if (Auth::check()) { Auth::logout(); }
        return redirect('register');
    }

    /**
     * DEcline invitation via email.
     *
     * @param  $id - Invitation ID
     * @return \Illuminate\Http\Response
     */
    public function declineEmailInvitation($id)
    {
        $invitation = Invitation::find($id);

        // Check if invitation exists
        if (!$invitation) {
			return view('errors/invitation')->with('data', ['image' => 'invalid-invitation.png', 'title' => 'Invalid invitation', 'message' => 'This project invitation is no longer valid. Please try another one or contact the person who sent you an invitation.']);
        }

        // Check if invitation was already accepted
        if ($invitation->status == 'accepted') {
            return  view('errors/invitation')->with('data', ['image' => 'accepted-invitation.png', 'title' => 'Accepted invitation', 'message' => 'This project invitation was already accepted. You may try leaving the project instead.']);
        }

        // Check if invitation was already declined
        if ($invitation->status == 'declined') {
			return view('errors/invitation')->with('data', ['image' => 'declined-invitation.png', 'title' => 'Declined invitation', 'message' => 'This project invitation was already declined. Please try another one or contact the person who sent you an invitation.']);
        }

        // Check if invitation was already expired
        if ($invitation->status == 'expired' || Carbon::create($invitation->expiration)->lessThan(now())) {
            if ($invitation->status != 'expired') {
                Invitation::where('id', $id)->update(['status' => 'expired']);
            }

            return view('errors/invitation')->with('data', ['image' => 'expired-invitation.png', 'title' => 'Expired invitation', 'message' => 'This project invitation has already expired last <strong>' . Carbon::create($invitation->expiration)->isoFormat('MMM Do, YYYY') . '</strong>. It can\'t be declined at this state.']);
        }

        Invitation::where('id', $id)->update(['status' => 'declined']);

		Self::sendDeclinedNotification($invitation, 'declined');

		return view('errors/invitation')->with('data', ['image' => 'declined-invitation.png', 'title' => 'Declined invitation', 'message' => 'This project invitation was already declined. Please try another one or contact the person who sent you an invitation.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function show(Invitation $invitation)
    {
        return Invitation::with('sender', 'project')->find(request('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitation $invitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitation $invitation)
    {
        $message = '';
        if (request('id')) {
            if (request('action') == 'accept') { // accept an invitation and redirect to project
                $invitation = Invitation::find(request('id'));

				if (!Team::where(['user_id' => auth()->user()->id, 'project_id' => $invitation->project_id])->exists()) { // check if already a member of the project
					$this->storeMember($invitation->project_id, $invitation->user_id);
				}

				Invitation::where('id', request('id'))->update(['status' => 'accepted']);

                $project = Project::find($invitation->project_id);

                return '/project/' . $project->token;
            } else if (request('action') == 'decline') { // decline an invitation
                Invitation::where('id', request('id'))->update(['status' => 'declined']);

                $message = 'Invitation Declined Successfully!';
				Self::sendDeclinedNotification(Invitation::find(request('id')), 'declined');
            } else { // delete an invitation
				$invitation = Invitation::find(request('id'));

				if ($invitation->status == 'new') {
					Self::sendDeclinedNotification($invitation, 'deleted');
				}

                Invitation::destroy(request('id'));

                $message = 'Invitation Deleted Successfully!';
            }
        } else {
            if (request('action') == 'accept') { // accept all new invitation
                $invitations = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->get();

                // add to project teams
                foreach ($invitations as $invitation) {
					if (!Team::where(['user_id' => auth()->user()->id, 'project_id' => $invitation->project_id])->exists()) { // check if already a member of the project
						$this->storeMember($invitation->project_id, $invitation->user_id);
					}

					Invitation::where('id', $invitation->id)->update(['status' => 'accepted']);
                }
				$message = 'Invitations Accepted Successfully!';
            } else if (request('action') == 'decline') { // decline an invitation
                $invitations = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->get();

                // decline and send notifications
                foreach ($invitations as $invitation) {
					Invitation::where('id', $invitation->id)->update(['status' => 'declined']);

					Self::sendDeclinedNotification($invitation, 'declined');
                }
                $message = 'Invitations Declined Successfully!';
            } else { // delete an invitation
                $invitations = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->get();

				// send notifications
                foreach ($invitations as $invitation) {
					Self::sendDeclinedNotification($invitation, 'deleted');
                }

                if (request('status')) { // delete all invitation received by a user depending on status
                    Invitation::where(['recipient_id' => auth()->user()->id, 'status' => request('status')])->delete();
                } else { // delete all invitations received by a user
                    Invitation::where(['recipient_id' => auth()->user()->id])->delete();
                }

                $message = 'Invitations Deleted Successfully!';
            }
        }

        $new = Invitation::where(['recipient_id' => auth()->user()->id, 'status' => 'new'])->count();

        return ['status' => 'success', 'message' => $message, 'data' => $new];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitation $invitation)
    {
        //
    }

	public function sendDeclinedNotification($invitation, $action) {
		$activityRecord = [
			'user_id' => $invitation->recipient_id,
			'project_id' => $invitation->project_id,
			'recipient_id' => $invitation->user_id,
			'title' => $action . ' your invitation.',
			'message' => $action . ' your project invitation.',
			'icon' => 'mdi-email-remove-outline',
			'event' => 'danger'
		];
		$this->sendNotification($activityRecord);
	}
}
