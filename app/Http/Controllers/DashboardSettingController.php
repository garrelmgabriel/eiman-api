<?php

namespace App\Http\Controllers;

use App\Models\DashboardSetting;
use Illuminate\Http\Request;

class DashboardSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DashboardSetting $dashboardSetting
     * @return \Illuminate\Http\Response
     */
    public function show(DashboardSetting $dashboardSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DashboardSetting $dashboardSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(DashboardSetting $dashboardSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DashboardSetting $dashboardSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DashboardSetting $dashboardSetting)
    {
		if (request('theme')) {
			DashboardSetting::where('user_id', auth()->user()->id)->update(['theme_color' => request('theme'), 'accent_color' => request('accent')]);
			return;
		}

		if (request('key') == 'statistics') {
			DashboardSetting::where('user_id', auth()->user()->id)->update([request('key') => json_encode(explode(',', request('value')))]);
			return ['status' => 'success', 'message' => 'Statistics Display Updated Successfully!'];
		}

		DashboardSetting::where('user_id', auth()->user()->id)->update([request('key') => request('value')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DashboardSetting $dashboardSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(DashboardSetting $dashboardSetting)
    {
        //
    }
}
