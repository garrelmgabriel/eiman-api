<?php

namespace App\Http\Controllers;

use JavaScript;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Project;
use App\Traits\TagTrait;
use App\Traits\TaskTrait;
use App\Traits\TeamTrait;
use App\Models\Automation;
use App\Events\ProjectEvent;
use App\Traits\SectionTrait;
use Illuminate\Http\Request;
use App\Models\ProjectSetting;
use App\Traits\InvitationTrait;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    use InvitationTrait;
    use TeamTrait;
    use SectionTrait;
    use TagTrait;
    use TaskTrait;

    /**
     * Display a list of the resource with statistics on a page.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $statistics = [];

        // total project count
        $statistics['total_projects'] = User::with('projects')->find(auth()->user()->id)->projects->count();

        // total active project count
        $statistics['active_projects'] = User::with(['projects' => function($query) {
            $query->where('status', 'active');
        }])->find(auth()->user()->id)->projects->count();

        // total archived project count
        $statistics['archived_projects'] = User::with(['projects' => function($query) {
            $query->where('status', 'archived');
        }])->find(auth()->user()->id)->projects->count();

        // total owned project count
        $statistics['owned_projects'] = User::with(['projects' => function($query) {
            $query->where('user_level', 1);
        }])->find(auth()->user()->id)->projects->count();

        JavaScript::put([
            'PAGE' => 'projects',
        ]);

        return view('projects')->with('statistics', $statistics);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = request('status');
        $name = request('name');

        $projects = User::with(['projects' => function($query) use ($status, $name) {
			if ($name) {
				$query->where('name', 'LIKE', '%' . $name . '%');
			}

            if ($status == 'active') {
                $query->where('status', 'active');
            } else if ($status == 'archived') {
                $query->where('status', 'archived');
            } else if ($status == 'trashed') {
                $query->where('status', 'trashed');
            } else if ($status == 'owned') {
                $query->where('user_level', 1);
            } else if ($status == 'allowed') {
                $query->where('status', 'active')->whereIn('user_level', [0, 1, 2]);
            }

			$query->orderby('is_favorite', 'DESC');
        }])->find(auth()->user()->id)->projects;

        return $projects;
    }

    /**
     * Display a list of all the active projects including its tags.
     *
     * @return \Illuminate\Http\Response
     */
	public function getActiveProjectsWithTags(){
		return User::with(['projects' => function($query) {
			$query->where('projects.status', 'active')
					->with('tags');
        }])->find(auth()->user()->id)->projects;
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'project-title' => 'required|string|max:50'
        ]);

        try {
            DB::beginTransaction();

            $project = Project::create([
                'token' => substr(md5(now() . request('project-title')), 0, 8),
                'name' => request('project-title'),
                'description' => request('project-description'),
                'icon' => request('project-icon'),
                'color' => request('project-color'),
            ]);

            // Log newly created project to user
            $this->storeMember($project->id);

            // Create Project Settings
            ProjectSetting::create(['project_id' => $project->id]);

            // Create Project Automations
            Automation::create(['project_id' => $project->id]);

            // Store Invitations and Send Emails if has value
            if (request('project-invitation-emails')) {
                $this->sendInvitation($project->id, $project->name, request('project-invitation-emails'), request('project-invitation-message'));
            }

            // Store Sections if has values
            if (request('project-sections')) {
                $this->storeSections($project->id, request('project-sections'));
            }

            // Store Sections if has values
            if (request('project-tags')) {
                $this->storeTags($project->id, request('project-tags'));
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        $data = User::with(['projects' => function($query) use ($project) {
                    $query->where('projects.id', $project->id);
                }])->find(auth()->user()->id)->projects;

        return ['status' => 'success', 'message' => 'Project Created Successfully!', 'data' => $data];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return Project::with(['sections' => function($query) {
                        $query->where('status', 'active')
                            ->orderBy('sections.sequence', 'ASC');
                    }])->with('tags')->find(request('id'));
    }

    /**
     * View a certain resource on a page.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($token)
    {
        $project = Project::with('announcement')->where('token', $token)->first();

        // Check of the project exists
        if (!$project) {
            return view('errors/404');
        }

        // Check if a user is a member of the project
        if (!Team::where(['user_id' => auth()->user()->id, 'project_id' => $project->id])->exists() && auth()->user()->user_level == 'user') {
            return view('errors/403');
        }

        // Get the role of the logged in user
        $member = User::with(['membership' => function($query) use ($project) {
                        $query->where('project_id', $project->id);
                    }])->find(auth()->user()->id)->membership;

		$project['membership'] = $member->first();

        JavaScript::put([
            'PAGE' => 'specific_project',
            'PROJECT_ID' => $project->id,
            'PROJECT_STATUS' => $project->status,
            'USER_ID' => $member[0]->user_id,
            'USER_ROLE' => $member[0]->user_level
        ]);

        return view('project')->with('project', $project);
    }

    /**
     * Update the basic info (icon, title, description) of specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateInfo(Request $request)
    {
        if (request('key') == 'name') {
            $validated = $request->validate([
                'key' => 'required|string|max:50'
            ]);
        }

        Project::where('id', request('project-id'))->update([request('key') => request('value')]);

		broadcast(new ProjectEvent('update-info', request('project-id')))->toOthers();

        return ['status' => 'success', 'message' => 'Project info was updated successfully!'];
    }

    /**
     * Update the status of specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStatus()
    {
		if (request('status') == 'active') {
			Task::where('project_id', request('id'))->update(['visible' => true]);
		} else {
			Task::where('project_id', request('id'))->update(['visible' => false]);
		}

        Project::where('id', request('id'))->update(['status' => request('status')]);

		broadcast(new ProjectEvent('update-status', request('id')))->toOthers();

        return ['status' => 'success', 'message' => 'Project status was updated successfully!'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validated = $request->validate([
            'project-title' => 'required|string|max:50'
        ]);

        try {
            DB::beginTransaction();

            Project::where('id', request('project-id'))->update([
                'name' => request('project-title'),
                'description' => request('project-description'),
                'icon' => request('project-icon'),
                'color' => request('project-color'),
            ]);

            // Store Invitations and Send Emails if has value
            if (request('project-invitation-emails')) {
                $this->sendInvitation(request('project-id'), request('project-title'), request('project-invitation-emails'), request('project-invitation-message'));
            }

            // Store Sections if has values
            if (request('project-sections')) {
                $this->storeSections(request('project-id'), request('project-sections'));
            }

            // Store Sections if has values
            if (request('project-tags')) {
                $this->storeTags(request('project-id'), request('project-tags'));
            }

			broadcast(new ProjectEvent('update-advanced', request('project-id')))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Project Updated Successfully!', 'data' => $request->all()];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTeam()
    {
        return Project::find(request('project-id'))->team;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBoard()
    {
        $sections = Project::with(['sections' => function($query) {
                        $query->where('status', request('section-status'))
                                ->orderBy('sections.sequence', 'ASC');
                    }])->find(request('project-id'))->sections;

        foreach ($sections as $section) {
            $section['tasks'] = $this->getSectionTasks($section->id, request('filters'));
        }

        return $sections;
    }

    /**
     * Display a listing of attachment of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAttachments()
    {
        return Project::find(request('project-id'))->attachments;
    }

    /**
     * Set specified resource as favorite in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function setFavorite()
    {
        Team::where('id', request('id'))->update(['is_favorite' => request('value')]);

        return ['status' => 'success', 'message' => 'Project was ' . (request('value') ? 'added to' : 'removed from') . ' favorites!'];
    }
}
