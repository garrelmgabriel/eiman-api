<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Task;
use App\Models\Watcher;
use App\Events\WatcherEvent;
use Illuminate\Http\Request;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class WatcherController extends Controller
{
	use NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Watcher::where(['user_id' => request('user-id'), 'task_id' => request('task-id')])->exists()) {
            return response()->json(['errors' => 'User was already a watcher of this task.'], 403);
        }

        try {
            DB::beginTransaction();

			$watcher = Watcher::create([
				'user_id' => request('user-id'),
				'task_id' => request('task-id'),
			]);

			$watch = Task::with(['watchers' => function($query) use ($watcher) {
							$query->where('watchers.id', $watcher->id);
						}])->find(request('task-id'))->watchers;

			$task = Task::find(request('task-id'));

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'User',
				'model_id' => request('user-id'),
				'title' => 'Assigned a watcher.',
				'message' => 'assigned <strong class="text-capitalize">$model</strong> as a watcher to <strong>$task_name</strong> task.',
				'icon' => 'mdi-eye-plus-outline',
				'event' => 'info'
			];

			// Log activity -- assign a watcher
			Log::create($activityRecord);

			// Send notification -- assign a watcher
			$activityRecord['message'] = 'assigned you as a watcher to <strong>' . $task->title . '</strong> task.';
			$activityRecord['recipient_id'] = request('user-id');
			$this->sendNotification($activityRecord);
			broadcast(new WatcherEvent('assign-watcher', $watcher->id))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Assigned Watcher Successfully!', 'data' =>  $watch[0]];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Watcher $watcher
     * @return \Illuminate\Http\Response
     */
    public function show(Watcher $watcher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Watcher $watcher
     * @return \Illuminate\Http\Response
     */
    public function edit(Watcher $watcher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Watcher $watcher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Watcher $watcher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Watcher $watcher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Watcher $watcher)
    {
        try {
            DB::beginTransaction();

			$watcher = Watcher::find(request('id'));
			broadcast(new WatcherEvent('unassign-watcher', request('id')))->toOthers();
			Watcher::destroy(request('id'));

			$task = Task::find($watcher->task_id);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'User',
				'model_id' => $watcher->user_id,
				'title' => 'Unassigned a watcher.',
				'message' => 'unassigned <strong class="text-capitalize">$model</strong> as a watcher from <strong>$task_name</strong> task.',
				'icon' => 'mdi-eye-remove-outline',
				'event' => 'danger'
			];

			// Log activity -- unassign a watcher
			Log::create($activityRecord);

			// Send notification -- unassign a watcher
			$activityRecord['message'] = 'unassigned you as a watcher from <strong>' . $task->title . '</strong> task.';
			$activityRecord['recipient_id'] = $watcher->user_id;
			$this->sendNotification($activityRecord);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Watcher User Successfully!'];
    }
}
