<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Tag;
use App\Models\Task;
use App\Models\TaskTag;
use App\Events\TaskTagEvent;
use Illuminate\Http\Request;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class TaskTagController extends Controller
{
	use NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (TaskTag::where(['tag_id' => request('tag-id'), 'task_id' => request('task-id')])->exists()) {
            return response()->json(['errors' => 'Tag was already assigned to this task.'], 403);
        }

        try {
            DB::beginTransaction();

			$tag = TaskTag::create([
				'task_id' => request('task-id'),
				'tag_id' => request('tag-id'),
			]);

			$taskTag = Task::with(['tags' => function($query) use ($tag) {
							$query->where('tags.id', $tag->tag_id);
						}])->find(request('task-id'))->tags;

			$task = Task::find(request('task-id'));

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'Tag',
				'model_id' => request('tag-id'),
				'title' => 'Assigned a tag.',
				'message' => 'assigned <strong class="text-lowercase">$model</strong> tag to <strong>$task_name</strong> task.',
				'icon' => 'mdi-tag-plus-outline',
				'event' => 'info'
			];

			// Log activity -- assign tag
			Log::create($activityRecord);

			// Send notification -- assign tag
			$activityRecord['message'] = 'assigned <strong class="text-lowercase">' . $taskTag[0]->name . '</strong> tag to <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group');
			broadcast(new TaskTagEvent('assign-tag', $tag->id))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Assigned Tag Successfully!', 'data' =>  $taskTag[0]];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TaskTag $taskTag
     * @return \Illuminate\Http\Response
     */
    public function show(TaskTag $taskTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TaskTag $taskTag
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskTag $taskTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\TaskTag $taskTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskTag $taskTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TaskTag $taskTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskTag $taskTag)
    {
        try {
            DB::beginTransaction();

			$tag = TaskTag::find(request('id'));
			broadcast(new TaskTagEvent('unassign-tag', $tag->id))->toOthers();
			TaskTag::destroy(request('id'));

			$task = Task::find($tag->task_id);
			$tag = Tag::find($tag->tag_id);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'Tag',
				'model_id' => $tag->id,
				'title' => 'Unassigned a tag.',
				'message' => 'unassigned <strong class="text-lowercase">$model</strong> tag from <strong>$task_name</strong> task.',
				'icon' => 'mdi-tag-remove-outline',
				'event' => 'danger'
			];

			// Log activity -- unassign tag
			Log::create($activityRecord);

			// Send notification -- unassign tag
			$activityRecord['message'] = 'unassigned <strong class="text-lowercase">' . $tag->name . '</strong> tag from <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group');

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Unassigned Tag Successfully!'];
    }
}
