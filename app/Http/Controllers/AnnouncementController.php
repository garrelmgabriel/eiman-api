<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Log;
use App\Models\Project;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementUser;
use App\Events\AnnouncementEvent;
use App\Traits\AnnouncementTrait;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class AnnouncementController extends Controller
{
	use AnnouncementTrait, NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'announcement-sdescription' => 'required|string|max:100'
        ]);

		try {
            DB::beginTransaction();

			$announcement = Announcement::create([
				'user_id' => auth()->user()->id,
				'project_id' => request('project-id'),
				'short_description' => request('announcement-sdescription'),
				'description' => request('announcement-description'),
				'type' => request('announcement-type'),
				'appointment_date' => request('announcement-type') == 'meeting' && request('appointment-date') ? Carbon::create(request('appointment-date'))->format('Y-m-d H:i:s') : null,
			]);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => request('project-id'),
				'title' => 'Posted a project announcement.',
				'message' => 'posted a project announcement.',
				'icon' => 'mdi-bullhorn-outline',
				'event' => 'info'
			];

			// Log activity -- post announcement
			Log::create($activityRecord);
			if (request('announcement-type') == 'meeting' && request('announcement-attendees')) {
				$this->manageAttendees(request('project-id'), $announcement->id, request('announcement-attendees'), Carbon::create(request('appointment-date'))->subMinutes(request('announcement-reminder'))->format('Y-m-d H:i:s'));
			}
			broadcast(new AnnouncementEvent('post-announcement', $announcement->id))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Announcement Posted Successfully!', 'data' => $announcement];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
		$announcement = Announcement::with('users')->where('project_id', request('project-id'))->first();

		if (request('action') == 'edit') {
			if ($announcement->user_id != auth()->user()->id && Project::find(request('project-id'))->owner->first()->id != auth()->user()->id) {
				return response()->json(['errors' => 'You don\'t have the rights to perform this action.'], 401);
			}
		}

        return $announcement;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
		//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        $validated = $request->validate([
            'announcement-sdescription' => 'required|string|max:100',
        ]);

		try {
            DB::beginTransaction();

			Announcement::where('project_id', request('project-id'))->update([
				'short_description' => request('announcement-sdescription'),
				'description' => request('announcement-description'),
				'appointment_date' => request('announcement-type') == 'meeting' && request('appointment-date') ? Carbon::create(request('appointment-date'))->format('Y-m-d H:i:s') : null,
			]);

			$announcement = Announcement::where('project_id', request('project-id'))->first();

			if (request('announcement-type') == 'meeting' && request('announcement-attendees')) {
				$this->manageAttendees(request('project-id'), $announcement->id, request('announcement-attendees'), Carbon::create(request('appointment-date'))->subMinutes(request('announcement-reminder'))->format('Y-m-d H:i:s'));
			}
			broadcast(new AnnouncementEvent('update-announcement', $announcement->id))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Announcement Updated Successfully!', 'data' => $announcement];
    }

    /**
     * Snooze the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function snooze(Request $request, Announcement $announcement)
    {
		$announcement = AnnouncementUser::find(request('announcement-user-id'));

		AnnouncementUser::where('id', request('announcement-user-id'))->update(['alarm_reminder' => Carbon::create($announcement->alarm_reminder)->addMinutes(5)->format('Y-m-d H:i:s')]);

        return ['status' => 'success', 'message' => 'Announcement Reminder was Snoozed!'];
    }

    /**
     * Dismiss the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function dismiss(Request $request, Announcement $announcement)
    {
		AnnouncementUser::where('id', request('announcement-user-id'))->update(['alarm_reminder' => null]);

        return ['status' => 'success', 'message' => 'Announcement Reminder Dismissed Successfully!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
		try {
            DB::beginTransaction();

			$attendees = AnnouncementUser::where('announcement_id', request('id'))->get();
			AnnouncementUser::where('announcement_id', request('id'))->delete();
			broadcast(new AnnouncementEvent('delete-announcement', request('id')))->toOthers();
			Announcement::destroy(request('id'));

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => request('project-id'),
				'title' => 'Removed a project announcement.',
				'message' => 'removed a project announcement.',
				'icon' => 'mdi-bullhorn-outline',
				'event' => 'danger'
			];

			// Log activity -- remove announcement
			Log::create($activityRecord);

			// Send notification -- remove announcement
			foreach ($attendees as $attendee) {
				$activityRecord['recipient_id'] = $attendee->user_id;
				$this->sendNotification($activityRecord);
			}

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Announcement Deleted Successfully!'];
    }
}
