<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// accessible only for admin and super admin
        if (auth()->user()->user_level == 'user') {
            return view('errors/403');
        }

        return view('organization');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getUsers()
	{
		$status = request('status');
		$name = request('name');
		$pagination = request('pagination');

		return User::where('company_id', auth()->user()->company_id)
					->when($name, function ($query) use ($name) {
						$query->where(function($query) use ($name) {
							$query->orWhereRaw("CONCAT(firstname,' ',COALESCE(middlename, ''),' ',lastname) like ?", ["%{$name}%"])
                        		->orWhereRaw("CONCAT(lastname,' ',COALESCE(middlename, ''),' ',firstname) like ?", ["%{$name}%"])
								->orWhereRaw("CONCAT(firstname,' ',lastname) like ?", ["%{$name}%"])
                        		->orWhereRaw("CONCAT(lastname,' ',firstname) like ?", ["%{$name}%"]);
						});
					})->when($status != 'all', function ($query) use ($status) {
						$query->where('status', $status);
					})->paginate($pagination);
	}

}

