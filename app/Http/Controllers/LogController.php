<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$filters = request()->all();

		return Log::when($filters['log-filter'], function ($query) use ($filters) {
						$query->where('logs.title', 'LIKE', '%' . $filters['log-filter'] . '%');
					})->when(array_key_exists('project-id', $filters), function ($query) use ($filters) { // project logs
						$query->where('project_id', $filters['project-id']);
					})->when(array_key_exists('task-id', $filters), function ($query) use ($filters) { // task logs
						$query->where('task_id', $filters['task-id']);
					})->when(array_key_exists('user-id', $filters), function ($query) use ($filters) { // user logs
						$query->where('user_id', $filters['user-id']);
					})
					->orderBy('created_at', 'DESC')->paginate(15);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Log  $Log
     * @return \Illuminate\Http\Response
     */
    public function show(Log $Log)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Log  $Log
     * @return \Illuminate\Http\Response
     */
    public function edit(Log $Log)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Log  $Log
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Log $Log)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Log  $Log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $Log)
    {
        //
    }
}
