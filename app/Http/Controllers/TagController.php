<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Project;
use App\Traits\TagTrait;
use Illuminate\Http\Request;

class TagController extends Controller
{
    use TagTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tag::where('project_id', request('project-id'))->get();
    }

    /**
     * Manage resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage(Request $request)
    {
        $this->storeTags(request('project-id'), request('project-tags'));

        $data = Project::with('tags')->find(request('project-id'))->tags;

        return ['status' => 'success', 'message' => 'Project Tags Updated Successfully!', 'data' => $data];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $Tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $Tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $Tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $Tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $Tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $Tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $Tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $Tag)
    {
        //
    }
}
