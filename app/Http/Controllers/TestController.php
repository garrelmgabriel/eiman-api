<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use App\Models\Project;
use App\Models\Section;
use App\Models\Feedback;
use App\Events\TeamEvent;
use App\Traits\TaskTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class TestController extends Controller
{
    use TaskTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// return User::all();
		$attachment =  DB::connection('live')->table('attachments')
						->select('*')->orderBy('attachments.id', 'asc')->find(9827);

		dd(Http::get('http://146.88.68.206:2040/storage/' . $attachment->path));
    }
}
