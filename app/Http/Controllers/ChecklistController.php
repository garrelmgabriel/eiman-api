<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Task;
use App\Models\Project;
use App\Models\Section;
use App\Traits\LogTrait;
use App\Events\TaskEvent;
use App\Models\Checklist;
use App\Traits\TaskTrait;
use App\Events\SectionEvent;
use Illuminate\Http\Request;
use App\Events\ChecklistEvent;
use App\Traits\NotificationTrait;
use Illuminate\Support\Facades\DB;

class ChecklistController extends Controller
{
	use TaskTrait, LogTrait, NotificationTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'key' => 'required|string|max:300'
        ]);

        try {
            DB::beginTransaction();

			$sequence = Checklist::where('task_id', request('task-id'))->orderBy('sequence', 'DESC')->limit(1)->value('sequence');

			$sequence = $sequence ? $sequence : 0;

			$checklist = Checklist::create([
				'task_id' => request('task-id'),
				request('key') => request('value'),
				'sequence' => $sequence + 10,
			]);

			$task = Task::find(request('task-id'));

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'model' => 'Checklist',
				'model_id' => $checklist->id,
				'title' => 'Added a checklist.',
				'message' => 'added <strong>$model</strong> checklist to <strong>$task_name</strong> task.',
				'icon' => 'mdi-format-list-checks',
				'event' => 'info'
			];

			// Log activity -- store a checklist
			Log::create($activityRecord);

			// Send notification -- store a checklist
			$activityRecord['message'] = 'added <strong>' . substr($checklist->title, 0, 50) . '</strong> checklist to <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group');
			broadcast(new ChecklistEvent(request('task-id')))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Checklist Created Successfully!', 'data' => $checklist];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Checklist  $checklist
     * @return \Illuminate\Http\Response
     */
    public function show(Checklist $checklist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Checklist $checklist
     * @return \Illuminate\Http\Response
     */
    public function edit(Checklist $checklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Checklist $checklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklist $checklist)
    {
		if (request('key') == 'title') {
			$validated = $request->validate([
				'key' => 'required|string|max:300'
			]);
		}

        try {
            DB::beginTransaction();
			$checklist = Checklist::with('task')->find(request('id'));

			if (request('key') == 'status') {
				$activityRecord = [
					'user_id' => auth()->user()->id,
					'project_id' => $checklist->task->project_id,
					'task_id' => $checklist->task->id,
					'model' => 'Checklist',
					'model_id' => request('id'),
					'title' => (request('value') == 'checked' ? 'Completed' : 'Reopened') . ' a checklist.',
					'message' => (request('value') == 'checked' ? 'completed' : 'reopened') . ' <strong>$model</strong> checklist from <strong>$task_name</strong> task.',
					'icon' => 'mdi-format-list-checks',
					'event' => request('value') == 'checked' ? 'success' : 'info',
				];

				// Log activity -- update checklist status
				Log::create($activityRecord);

				// Send notification -- update checklist status
				$activityRecord['message'] = (request('value') == 'checked' ? 'completed' : 'reopened') . ' <strong>' . substr($checklist->title, 0, 50) . '...</strong> checklist from <strong>' . $checklist->task->title . '</strong> task.';
				$this->sendNotification($activityRecord, 'group');
			}

			Checklist::where('id', request('id'))->update([request('key') => (request('value') == 'null' ? null : request('value'))]);
			broadcast(new ChecklistEvent($checklist->task_id))->toOthers();

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}

        return ['status' => 'success', 'message' => 'Checklist Updated Successfully!'];
    }

    /**
     * Update the sequence of the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateSequence(Request $request)
    {
        try {
            DB::beginTransaction();

			// Checklists IDs
			$CSID = request('current-sequence-id');
			$BSID = request('before-sequence-id');
			$ASID = request('after-sequence-id');

			// Get the current sequence of respective IDs
			$BS = $BSID ? Checklist::where('id', $BSID)->value('sequence') : null; // Sequence of the section before the specified resource
			$AS = $ASID ? Checklist::where('id', $ASID)->value('sequence') : null; // Sequence of the section after the specified resource
			$NS = null; // new sequence to be assigned to the specified resource

			if ($AS && $BS) { // if the specified resource fits in between of the two concurrent sections
				$NS = ($AS + $BS) / 2;

				if ($AS == $BS) {
					$NS = $AS + 5;
				}
			} else if (!$AS && $BS) { // if the two concurrent sections have the same sequence, just add 0.2 to the sequence of the specified resource
				$NS = $BS + 10;
			} else if ($AS && !$BS) { // if the specified resource was moved at the very least of the collection
				$NS = $AS - 10;
			} else if (!$AS && !$BS) { // if the specified resource was moved at the very beggining of the collection
				$NS = 10;
			}

			if ($AS && ($NS > $AS)) { // if the new sequence of the specified resource is greater than the section after it then sort it to fit the logic by adding 0.2 greater than the specified resource
				$checklists = Checklist::where(['sequence' => $AS, 'task_id' => request('task-id')])->where('id', '>=', $ASID)->get();

				if ($checklists) {
					$inc = 5;
					foreach ($checklists as $checklist) {
						$sequence = $NS + $inc;

						Checklist::where('id', $checklist->id)->update(['sequence' => $sequence]);
						$inc += 5;
					}
				}
			}

			Checklist::where('id', $CSID)->update(['sequence' => $NS]);
			broadcast(new ChecklistEvent(request('task-id')))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Checklist sequence was updated successfully!'];
    }

    /**
     * Convert checklist to task
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function convert(Request $request)
    {
        try {
            DB::beginTransaction();

			$data = [];
			$sectionId = request('section-id');

			// create section
			if ($sectionId == '0') {
				$validated = $request->validate([
					'new-section-name' => 'required|string|max:50',
				]);

				$sequence = Section::where('project_id', request('project-id'))->orderBy('sequence', 'DESC')->limit(1)->value('sequence');

				$sequence = $sequence ? $sequence : 0;

				$section = Section::create([
					'project_id' => request('project-id'),
					'name' => request('new-section-name'),
					'color' => request('section-color'),
					'sequence' => $sequence + 1,
				]);

				$sectionId = $section->id;

				$data['section'] = $section;
				$this->createSectionLog(request('project-id'), $sectionId);

				broadcast(new SectionEvent('create',  $sectionId))->toOthers();
			}

			$sequence = Task::where('section_id', $sectionId)->orderBy('sequence', 'DESC')->limit(1)->value('sequence');

			$sequence = $sequence ? $sequence : 0;

			$data['task'] =  Task::create([
				'project_id' => request('project-id'),
				'section_id' => $sectionId,
				'token' => substr(md5(now() . request('task-name')), 0, 8),
				'sequence' => $sequence + 1,
				'title' => request('task-name'),
				'status' => 'open'
			]);

			$data['task'] = $this->parseBasicTask($data['task']);

			$projectStatistics = Project::find(request('project-id'))->statistics;

			// Log activity -- convert checklist to task
			Log::create([
				'user_id' => auth()->user()->id,
				'project_id' => request('project-id'),
				'task_id' => $data['task']->id,
				'title' => 'Converted checklist to task.',
				'message' => 'converted <strong>$task_name</strong> checklist to task.',
				'icon' => 'mdi-format-list-checks',
				'event' => 'success'
			]);
			broadcast(new TaskEvent('add-task', $data['task']['id']))->toOthers();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollback();
            return $th;
        }

        return ['status' => 'success', 'message' => 'Converted Checklist Successfully!', 'data' => $data, 'projectStatistics' => $projectStatistics];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Checklist $checklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklist $checklist)
    {
        try {
            DB::beginTransaction();

			$checklist = Checklist::find(request('id'));
			Checklist::destroy(request('id'));
			broadcast(new ChecklistEvent($checklist->task_id))->toOthers();

			$task = Task::find($checklist->task_id);

			$activityRecord = [
				'user_id' => auth()->user()->id,
				'project_id' => $task->project_id,
				'task_id' => $task->id,
				'title' => 'Removed a checklist.',
				'message' => 'removed a checklist from <strong>$task_name</strong> task.',
				'icon' => 'mdi-format-list-checks',
				'event' => 'danger'
			];

			// Log activity -- remove a checklist
			Log::create($activityRecord);

			// Send notification -- remove a checklist
			$activityRecord['message'] = 'removed a checklist from <strong>' . $task->title . '</strong> task.';
			$this->sendNotification($activityRecord, 'group');

			DB::commit();
		} catch (Throwable $th) {
			DB::rollback();
			return $th;
		}
    }
}
