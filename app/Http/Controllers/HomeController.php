<?php

namespace App\Http\Controllers;

use JavaScript;
use Carbon\Carbon;
use App\Models\Team;
use App\Models\User;
use App\Traits\TeamTrait;
use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Models\DashboardSetting;

class HomeController extends Controller
{
    use TeamTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Check origin if invitation
        if (session('invitation')) {
            $invitation = session('invitation');

            if (auth()->user()->email === $invitation->email) {
                $this->storeMember($invitation->project_id, $invitation->user_id);

                Invitation::where('id', $invitation->id)->update(['recipient_id' => auth()->user()->id,'status' => 'accepted']);
            }

            JavaScript::put(['JOINED' => true]);
            session()->forget('invitation');
        }

        JavaScript::put([
            'PAGE' => 'home',
            'USER_ID' => auth()->user()->id,
        ]);

        return view('home');
    }

    /**
     * Get the statistics specified by the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboardStatistics()
    {
        $statistics = [];

        $counts = User::with(['projects' => function($query) {
                        $query->where('status', 'active');
                    }])->find(auth()->user()->id)->projects->count();

        if ($counts) {
			$statCategories = auth()->user()->dashboardSettings->statistics;

            // total projects count
			if (in_array('stat_total_projects', $statCategories)) {
				$statistics['total_projects']['color'] = 'theme';
				$statistics['total_projects']['icon'] = 'fe-layers';
				$statistics['total_projects']['count'] = User::with('projects')->find(auth()->user()->id)->projects->count();
			}

			// total owned projects count
			if (in_array('stat_owned_projects', $statCategories)) {
				$statistics['total_owned_projects']['color'] = 'accent';
				$statistics['total_owned_projects']['icon'] = 'fe-star';
				$statistics['total_owned_projects']['count'] = User::with(['projects' => function($query) {
					$query->where('user_level', 1);
				}])->find(auth()->user()->id)->projects->count();
			}

			// total active projects count
			if (in_array('stat_active_projects', $statCategories)) {
				$statistics['total_active_projects']['color'] = 'primary';
				$statistics['total_active_projects']['icon'] = 'fe-activity';
				$statistics['total_active_projects']['count'] = User::with(['projects' => function($query) {
					$query->where('status', 'active');
				}])->find(auth()->user()->id)->projects->count();
			}

			// total project groups count -- TODO
			if (in_array('stat_total_groups', $statCategories)) {
				$statistics['total_project_groups']['color'] = 'info';
				$statistics['total_project_groups']['icon'] = 'fe-box';
				$statistics['total_project_groups']['count'] = 0;
			}

			// total archived projects count
			if (in_array('stat_archived_projects', $statCategories)) {
				$statistics['total_archived_projects']['color'] = 'secondary';
				$statistics['total_archived_projects']['icon'] = 'fe-archive';
				$statistics['total_archived_projects']['count'] = User::with(['projects' => function($query) {
					$query->where('status', 'archived');
				}])->find(auth()->user()->id)->projects->count();
			}

			// total tasks today count
			if (in_array('stat_tasks_today', $statCategories)) {
				$statistics['total_tasks_today']['color'] = 'warning';
				$statistics['total_tasks_today']['icon'] = 'fe-airplay';
				$statistics['total_tasks_today']['count'] = User::with(['assigned' => function($query) {
							$query->whereDate('tasks.start_date', Carbon::today());
						}])->find(auth()->user()->id)->assigned->count();
			}

            // total tasks assigned count
			if (in_array('stat_total_tasks', $statCategories)) {
				$statistics['total_tasks']['color'] = 'pink';
				$statistics['total_tasks']['icon'] = 'fe-user';
				$statistics['total_tasks']['count'] = User::with('assigned')->find(auth()->user()->id)->assigned->count();
			}

            // total active tasks count
			if (in_array('stat_active_tasks', $statCategories)) {
				$statistics['total_active_tasks']['color'] = 'purple';
				$statistics['total_active_tasks']['icon'] = 'fe-list';
				$statistics['total_active_tasks']['count'] = User::with(['assigned' => function($query) {
							$query->where('tasks.status', 'open');
						}])->find(auth()->user()->id)->assigned->count();
			}

            // total completed tasks count
			if (in_array('stat_completed_tasks', $statCategories)) {
				$statistics['total_completed_tasks']['color'] = 'success';
				$statistics['total_completed_tasks']['icon'] = 'fe-check-circle';
				$statistics['total_completed_tasks']['count'] = User::with(['assigned' => function($query) {
							$query->where('tasks.status', 'completed');
						}])->find(auth()->user()->id)->assigned->count();
			}

			// total overdue tasks count
			if (in_array('stat_overdue_tasks', $statCategories)) {
				$statistics['total_overdue_tasks']['color'] = 'danger';
				$statistics['total_overdue_tasks']['icon'] = 'fe-calendar';
				$statistics['total_overdue_tasks']['count'] = User::with(['assigned' => function($query) {
							$query->where('tasks.status', 'overdue');
						}])->find(auth()->user()->id)->assigned->count();
			}
        }

        return ['statistics' => $statistics, 'counts' => $counts];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProjects()
    {
        $projects = null;
        $category = DashboardSetting::where('user_id', auth()->user()->id)->first()->project_display;

		$projects = User::with(['projects' => function($query) use ($category) {
            if ($category == 'owned') {
                $query->where('user_level', 1);
            }

            if ($category == 'favorite') {
                $query->where('is_favorite', 1);
            }
			$query->where('status', 'active')->orderBy('teams.created_at', 'DESC')->limit(10);
        }])->find(auth()->user()->id)->projects;

        return $projects;
    }
}
