<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'title',
        'path',
        'extension',
    ];

    /**
     * Get the user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
