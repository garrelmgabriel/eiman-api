<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'project_id',
        'recipient_id',
        'email',
        'message',
        'status',
        'expiration',
    ];

    /**
     * Get the sender's info
     */
    public function sender() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the recipients's info
     */
    public function recipient() {
        return $this->belongsTo(User::class, 'recipient_id');
    }
}
