<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;

	protected $appends = ['fullname'];
    public $table = 'feedbacks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'message',
		'anonymous',
    ];

    /**
     * Get the feedback sender's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

	/**
     * Get the feedback sender's fullname
     */
	public function getFullnameAttribute(){
		$fullname = null;

		if ($this->user->firstname && $this->user->lastname) {
			$fullname = $this->user->firstname . ' ' . $this->user->middlename . ' ' . $this->user->lastname ;
		}

		return ucwords($fullname);
	}
}
