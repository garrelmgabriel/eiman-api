<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use HasFactory;

	protected $appends = ['simplified_reminder'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'project_id',
        'short_description',
        'description',
        'type',
        'appointment_date',
    ];

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the creator's info
     */
    public function creator() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the users' info
     */
    public function users() {
        return $this->belongsToMany(User::class, 'announcement_user')->withPivot('id', 'status', 'is_creator', 'alarm_reminder');
    }

	/**
	 * Get announcement reminder
	 */
	public function getSimplifiedReminderAttribute() {
		$value = null;
		$appointmentDate = Carbon::parse($this->appointment_date);
		if ($this->users) {
			foreach ($this->users as $user) {
				if (auth()->user() && $user->id == auth()->user()->id) {
					$value = $appointmentDate->diffInMinutes(Carbon::parse($user->pivot->alarm_reminder));
				}
			}
		}

		return $value;
	}

}
