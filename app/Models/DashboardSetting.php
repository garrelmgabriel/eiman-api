<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardSetting extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'background',
        'time_format',
        'show_quotes',
        'show_notes',
        'color_scheme',
        'theme_color',
        'accent_color',
        'project_display',
		'statistics',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
	protected $casts = [
        'statistics' => 'array',
    ];

    /**
     * Get the user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
