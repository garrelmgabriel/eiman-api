<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
		'name',
		'has_project_limit',
		'project_limit',
		'has_user_limit',
		'user_limit',
    ];

    /**
     * Get package's companies
     */
    public function companies() {
        return $this->hasMany(Company::class);
    }
}
