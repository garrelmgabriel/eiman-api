<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

	protected $appends = ['parsed_message'];
	protected $with = ['project', 'task'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'recipient_id',
        'project_id',
        'task_id',
        'title',
        'message',
        'icon',
        'event',
        'is_read',
    ];

    /**
     * Get the user's info
     */
    public function sender() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Get the task's info
     */
    public function task() {
		return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    /**
     * Get the recipients's info
     */
    public function recipient() {
        return $this->belongsTo(User::class);
    }

	/**
	 * Get notification message
	 */
	public function getParsedMessageAttribute() {
		$sender = $this->sender;
		$message = $this->message;

		if ($sender) {
			return '<strong class="text-capitalize">' . $sender->full_name . '</strong> ' . $message;
		}

		return $message;
	}
}
