<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'color',
    ];

    /**
     * Get the user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the tasks' info
     */
    public function tasks() {
        return $this->belongsToMany(Task::class, 'task_pin')->where('visible', true);
    }
}
