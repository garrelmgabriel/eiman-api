<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    use HasFactory;

	protected $appends = ['all_emails'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'toast_alerts',
        'emails_for_project_invitations',
        'emails_for_task_assignments',
        'emails_for_comments_or_mentions',
        'emails_for_reminders',
        'emails_for_system_updates',
        'desktop_notif',
        'notif_sound',
    ];

    /**
     * Get the user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

	/**
	 * Get all email notification
	 */
	public function getAllEmailsAttribute() {
		if ($this->emails_for_project_invitations && $this->emails_for_task_assignments && $this->emails_for_comments_or_mentions && $this->emails_for_reminders && $this->emails_for_system_updates) {
			return 1;
		}

		return 0;
	}
}
