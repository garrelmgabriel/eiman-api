<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectSetting extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'enable_background',
        'background',
        'password_protected',
        'password',
        'solid_section_backgrounds',
    ];

    /**
     * Get the project's info
     */
    public function projects() {
        return $this->belongsTo(Project::class);
    }
}
