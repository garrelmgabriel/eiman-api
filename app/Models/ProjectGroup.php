<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectGroup extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
    ];

    /**
     * Get the user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the project's info
     */
    public function projects() {
        return $this->belongsToMany(Project::class, 'teams');
    }
}
