<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'task_id',
        'title',
        'status',
        'sequence',
    ];

    /**
     * Get the assigned user's info
     */
    public function user() {
        return $this->hasOne(User::class);
    }

    /**
     * Get the task's info
     */
    public function task() {
        return $this->belongsTo(Task::class);
    }
}
