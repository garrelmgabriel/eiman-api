<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'name',
        'color',
    ];

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the task's info
     */
    public function tasks() {
        return $this->hasMany(TaskTag::class);
    }
}
