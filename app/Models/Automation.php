<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Automation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'completed_section',
        'backlog_section',
        'backlog_duration',
    ];

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }
}
