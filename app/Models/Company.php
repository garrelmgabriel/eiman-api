<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
		'package_id',
		'name',
		'slug',
		'description',
		'contact',
		'email',
		'status',
		'subscription_duration',
		'subscription_date',
		'subscription_expiration',
    ];

    /**
     * Get company's package
     */
    public function package() {
        return $this->belongsTo(Package::class);
    }

    /**
     * Get company's users
     */
    public function users() {
        return $this->hasMany(User::class);
    }

    /**
     * Get company's active users
     */
    public function activeUsers() {
        return $this->hasMany(User::class)->where('status', 'active');
    }

    /**
     * Get company's disabled accounts
     */
    public function disabledUsers() {
        return $this->hasMany(User::class)->where('status', 'disabled');
    }
}
