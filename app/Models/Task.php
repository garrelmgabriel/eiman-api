<?php

namespace App\Models;

use Carbon\Carbon;
use PhpParser\Node\Expr\Assign;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;

	protected $appends = ['was_due'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'section_id',
        'sequence',
        'token',
        'title',
        'description',
        'start_date',
        'due_date',
        'status',
		'visible',
        'completed_at',
    ];

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the section's info
     */
    public function section() {
        return $this->belongsTo(Section::class);
    }

    /**
     * Get the attachments' info
     */
    public function attachments() {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Get the checklists' info
     */
    public function checklists() {
        return $this->hasMany(Checklist::class);
    }

    /**
     * Get the comments' info
     */
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get the assignees' info
     */
    public function assignees() {
        return $this->belongsToMany(User::class, 'assignments')->withPivot('id');
    }

    /**
     * Get the watchers' info
     */
    public function watchers() {
        return $this->belongsToMany(User::class, 'watchers')->withPivot('id');
    }

    /**
     * Get the tags' info
     */
    public function tags() {
        return $this->belongsToMany(Tag::class, 'task_tag')->withPivot('id');
    }

    /**
     * Get the pins' info
     */
    public function pins() {
        return $this->belongsToMany(Pin::class, 'task_pin');
    }

    /**
     * Get the notifications's info
     */
    public function notifications() {
        return $this->hasMany(Notification::class);
    }

    /**
     * Get the logs' info
     */
    public function logs() {
        return $this->hasMany(Log::class);
    }

	/**
	 * Check if the task was overdue before the completion date
	 */
	public function getWasDueAttribute() {
		$wasDue = false;

		if ($this->due_date && $this->completed_at && Carbon::create($this->due_date) < Carbon::create($this->completed_at)) {
			$wasDue = true;
		}

		return $wasDue;
	}
}
