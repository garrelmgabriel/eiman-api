<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

	protected $appends = ['initials', 'display_name', 'full_name', 'pronouns', 'color'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
		'company_id',
        'firstname',
        'middlename',
        'lastname',
        'nickname',
        'title',
        'email',
        'password',
        'avatar',
        'custom_status',
		'activity_status',
        'language',
        'timezone',
        'connected_accounts',
        'user_level',
        'status',
        'viewed_updates',
        'agree_to_terms',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get user's company
     */
    public function company() {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get all user's settings
     */
    public function settings() {
        return $this->hasOne(UserSetting::class);
    }

    /**
     * Get all user's dashboard settings
     */
    public function dashboardSettings() {
        return $this->hasOne(DashboardSetting::class, 'user_id');
    }

    /**
     * Get all user's feedbacks
     */
    public function feedbacks() {
        return $this->hasMany(Feedback::class);
    }

    /**
     * Get all user's exports
     */
    public function exports() {
        return $this->hasMany(Export::class);
    }

    /**
     * Get all user's sent notifications
     */
    public function sentNotifications() {
        return $this->hasMany(Notification::class, 'user_id');
    }

    /**
     * Get all user's received notifications
     */
    public function receivedNotifications() {
        return $this->hasMany(Notification::class, 'recipient_id');
    }

    /**
     * Get all user's activity
     */
    public function logs() {
        return $this->hasMany(Log::class);
    }

    /**
     * Get all user's logged in activity
     */
    public function userlogs() {
        return $this->hasMany(UserLog::class);
    }

    /**
     * Get all user's sent invitations
     */
    public function sentInvitations() {
        return $this->hasMany(Invitation::class, 'user_id');
    }

    /**
     * Get all user's received invitations
     */
    public function receivedInvitations() {
        return $this->hasMany(Invitation::class, 'recipient_id');
    }

    /**
     * Get all user's group
     */
    public function groups() {
        return $this->hasMany(ProjectGroup::class);
    }

    /**
     * Get all user's projects
     */
    public function membership() {
        return $this->hasMany(Team::class);
    }

    /**
     * Get all user's projects
     */
    public function projects() {
        return $this->belongsToMany(Project::class, 'teams')->withPivot('user_level', 'sequence', 'is_favorite');
    }

    /**
     * Get all user's pins
     */
    public function pins() {
        return $this->hasMany(Pin::class);
    }

    /**
     * Get all user's attachments
     */
    public function attachments() {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Get all user's comments
     */
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get all user's assigned checklists
     */
    public function checklist() {
        return $this->belongsToMany(Checklist::class);
    }

    /**
     * Get all user's assigned tasks
     */
    public function assigned() {
        return $this->belongsToMany(Task::class, 'assignments')->where('visible', true)->withPivot('id', 'is_new', 'created_at');
    }

    /**
     * Get all user's watched tasks
     */
    public function watched() {
        return $this->belongsToMany(Task::class, 'watchers')->withPivot('id', 'created_at')->where('visible', true);
    }

    /**
     * Get all announcements' involved
     */
    public function announcements() {
        return $this->belongsToMany(Announcement::class, 'announcement_user')->withPivot('id', 'status', 'is_creator', 'alarm_reminder');
    }

    /**
     * Get all owned announcements' involved
     */
    public function ownedAnnouncements() {
        return $this->hasMany(Announcement::class);
    }

    /**
     * Get all user's notes
     */
    public function notes() {
        return $this->hasMany(Note::class);
    }

	/**
	 * Get user's name initials
	 */
	public function getInitialsAttribute() {
		$initial = $this->email[0];

		if ($this->nickname) {
			$initial = $this->nickname[0];
		} else if ($this->firstname && $this->lastname) {
			$initial = $this->firstname[0] . $this->lastname[0];
		}

		return strtoupper($initial);
	}

	/**
	 * Get user's display name
	 */
	public function getDisplayNameAttribute() {
		$dName = $this->email;

		if ($this->nickname) {
			$dName = $this->nickname;
		} else if ($this->firstname) {
			$dName = $this->firstname;
		}

		return ucwords(($this->title ? $this->title . ' ' : '') . $dName);
	}

	/**
	 * Get user's full name
	 */
	public function getFullNameAttribute() {
		$fullName = null;

		if ($this->firstname && $this->lastname) {
			$fullName = $this->firstname . ' ' . $this->middlename . ' ' . $this->lastname ;
		}

		return ucwords($fullName);
	}

	/**
	 * Get user's full name
	 */
	public function getPronounsAttribute() {
		$pronouns = [
			'subject' => $this->display_name,
			'object' => $this->display_name,
			'adjective' => $this->display_name,
			'possessive' => $this->display_name,
			'reflexive' => $this->display_name,
		];

		if ($this->gender == 'male') {
			$pronouns = [
				'subject' => 'he',
				'object' => 'him',
				'adjective' => 'his',
				'possessive' => 'his',
				'reflexive' => 'himself',
			];
		} else if ($this->gender == 'female') {
			$pronouns = [
				'subject' => 'she',
				'object' => 'her',
				'adjective' => 'her',
				'possessive' => 'hers',
				'reflexive' => 'herself',
			];
		} else if ($this->gender == 'non-binary') {
			$pronouns = [
				'subject' => 'they',
				'object' => 'them',
				'adjective' => 'their',
				'possessive' => 'theirs',
				'reflexive' => 'themselves',
			];
		}

		return $pronouns;
	}

	/**
	 * Get user's set colors
	 */
	public function getColorAttribute() {
		if ($this->dashboardSettings) {
			return $this->dashboardSettings->theme_color;
		}

		return '#303476';
	}

}
