<?php

namespace App\Models;

use App\Models\Tag;
use App\Models\User;
use App\Models\Comment;
use App\Models\Section;
use App\Models\Checklist;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Log extends Model
{
    use HasFactory;

	protected $appends = ['parsed_message'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'project_id',
        'task_id',
        'model',
        'model_id',
        'title',
        'message',
		'icon',
        'event',
    ];

    /**
     * Get the user's info
     */
    public function sender() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the project's info
     */
    public function project() {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the task's info
     */
    public function task() {
        return $this->belongsTo(Task::class);
    }

	/**
	 * Get log message
	 */
	public function getParsedMessageAttribute() {
		$sender = $this->sender;
		$message = $this->message;

		// Supply task title to message
		if (str_contains($message, '$task_name')) {
			$task = $this->task;
			$message = str_replace('$task_name', $task->title, $message);
		}

		// Supply necessary model's information to message
		if ($this->model) {
			$model = $this->model;

			if (strtolower($model) == 'user') {
				$user = User::find($this->model_id);
				$name = '<strong class="text-capitalize">' . $user->display_name . '</strong>';

				if ($this->user_id == $this->model_id) {
					$name = '<strong>' . $user->pronouns['reflexive'] . '</strong>';
				}

				$message = str_replace('<strong class="text-capitalize">$model</strong>', $name, $message);
			} else if (strtolower($model) == 'section') {
				$section = Section::find($this->model_id);
				$message = str_replace('$model', ($section ? $section->name : '<span class="text-muted fst-italic">DELETED SECTION</span>'), $message);
			} else if (strtolower($model) == 'tag') {
				$tag = Tag::find($this->model_id);
				$message = str_replace('$model', ($tag ? $tag->name : '<span class="text-muted fst-italic">DELETED TAG</span>'), $message);
			} else if (strtolower($model) == 'checklist') {
				$checklist = Checklist::find($this->model_id);
				$message = str_replace('$model', ($checklist ? $checklist->title : '<span class="text-muted fst-italic">DELETED CHECKLIST</span>'), $message);
			} else if (strtolower($model) == 'comment') {
				$comment = Comment::find($this->model_id);
				$message = str_replace('$model', ($comment ? $comment->message : '<span class="text-muted fst-italic">DELETED COMMENT</span>'), $message);
			}
		}

		if ($sender) {
			return '<strong class="text-capitalize">' . $sender->display_name . '</strong> ' . $message;
		}
		return $message;
	}
}
