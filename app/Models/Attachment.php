<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

	protected $appends = ['uploader'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'task_id',
        'name',
        'title',
        'path',
        'extension',
    ];

    /**
     * Get the assigned user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the task's info
     */
    public function task() {
        return $this->belongsTo(Task::class);
    }

	/**
	 * Get uploader's info
	 */
	public function getUploaderAttribute() {
		return $this->user->full_name;
	}
}
