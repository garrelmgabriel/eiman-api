<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

	protected $appends = ['statistics'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'name',
        'description',
        'icon',
        'color',
        'status',
        'external_project',
    ];

    /**
     * Get the owner's info
     */
    public function owner() {
        return $this->belongsToMany(User::class, 'teams')->where('teams.user_level', '1');
    }

    /**
     * Get the announcement's info
     */
    public function announcement() {
        return $this->hasOne(Announcement::class)->with('users');
    }

    /**
     * Get the team's info
     */
    public function team() {
        return $this->belongsToMany(User::class, 'teams')->withPivot('user_level', 'duration', 'is_favorite');
    }

    /**
     * Get the groups's info
     */
    public function group() {
        return $this->belongsToMany(ProjectGroup::class, 'teams');
    }

    /**
     * Get the section's info
     */
    public function sections() {
        return $this->hasMany(Section::class);
    }

    /**
     * Get the notifications's info
     */
    public function notifications() {
        return $this->hasMany(Notification::class);
    }

    /**
     * Get the log's info
     */
    public function logs() {
        return $this->hasMany(Log::class);
    }

    /**
     * Get the automation's info
     */
    public function automation() {
        return $this->hasOne(Automation::class);
    }

    /**
     * Get the settings's info
     */
    public function settings() {
        return $this->hasOne(ProjectSetting::class);
    }

    /**
     * Get the tag's info
     */
    public function tags() {
        return $this->hasMany(Tag::class);
    }

    /**
     * Get the invitation's info
     */
    public function invitations() {
        return $this->hasMany(Invitation::class);
    }

    /**
     * Get the task's info
     */
    public function tasks() {
        return $this->hasMany(Task::class);
    }

    /**
     * Count the open task
     */
    public function totalActiveTasks() {
        return $this->hasMany(Task::class)->whereIn('status', ['open', 'completed', 'overdue'])->where('visible', true);
    }

    /**
     * Count the open task
     */
    public function openTasks() {
        return $this->hasMany(Task::class)->where('status', 'open')->where('tasks.visible', true);
    }

    /**
     * Count the completed task
     */
    public function completedTasks() {
        return $this->hasMany(Task::class)->where('status', 'completed')->where('tasks.visible', true);
    }

    /**
     * Count the overdue task
     */
    public function overdueTasks() {
        return $this->hasMany(Task::class)->where('status', 'overdue')->where('tasks.visible', true);
    }

    /**
     * Get the attachment's info
     */
    public function attachments() {
		return $this->hasManyThrough(Attachment::class, Task::class)->whereIn('tasks.status', ['open', 'completed', 'overdue'])->where('tasks.visible', true)->with('user');
    }

	/**
	 * Get project's statistics
	 */
	public function getStatisticsAttribute() {
		$statistics = [
			'open' => 0,
			'completed' => 0,
			'overdue' => 0,
			'total' => 0,
			'open_percent' => 0,
			'completed_percent' => 0,
			'overdue_percent' => 0,
		];

		$statistics['open'] = $this->openTasks->count();
		$statistics['completed'] = $this->completedTasks->count();
		$statistics['overdue'] = $this->overdueTasks->count();
		$statistics['total'] = $statistics['open'] + $statistics['completed'] + $statistics['overdue'];
		if ($statistics['total']) {
			$statistics['open_percent'] = round(($statistics['open'] / $statistics['total']) * 100, 2);
			$statistics['completed_percent'] = round(($statistics['completed'] / $statistics['total']) * 100, 2);
			$statistics['overdue_percent'] = round(($statistics['overdue'] / $statistics['total']) * 100, 2);
		}

		return $statistics;
	}
}
