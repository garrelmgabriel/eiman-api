<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'color',
        'content',
        'status',
        'is_pinned',
    ];

    /**
     * Get the assigned user's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
