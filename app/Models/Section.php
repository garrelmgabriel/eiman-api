<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'name',
        'description',
        'color',
        'sequence',
        'status',
    ];

    /**
     * Get the project's info
     */
    public function projects() {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Get the task's info
     */
    public function tasks() {
        return $this->hasMany(Task::class);
    }

    /**
     * Count the open task
     */
    public function totalActiveTasks() {
        return $this->hasMany(Task::class)->whereIn('status', ['open', 'completed', 'overdue']);
    }

    /**
     * Count the open task
     */
    public function openTasks() {
        return $this->hasMany(Task::class)->where('status', 'open');
    }

    /**
     * Count the completed task
     */
    public function completedTasks() {
        return $this->hasMany(Task::class)->where('status', 'completed');
    }

    /**
     * Count the overdue task
     */
    public function overdueTasks() {
        return $this->hasMany(Task::class)->where('status', 'overdue');
    }

	/**
	 * Get project's statistics
	 */
	public function getStatisticsAttribute() {
	}
}
