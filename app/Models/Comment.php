<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

	protected $appends = ['commenter'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'task_id',
        'reply_to',
        'message',
        'is_edited',
    ];

    /**
     * Get the commenter's info
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the task's info
     */
    public function task() {
        return $this->belongsTo(Task::class);
    }

	/**
	 * Get uploader's info
	 */
	public function getCommenterAttribute() {
		return $this->user->full_name;
	}
}
